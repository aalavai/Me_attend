// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
import 'react-native-gesture-handler';

// Import React and Component
import React,{ useState, createRef, useEffect, useRef } from 'react';

// Import Navigators from React Navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// Import Screens
import SplashScreen from './src/Screen/SplashScreen';
import LoginScreen from './src/Screen/LoginScreen';
import OTPscreen from './src/Screen/OTPscreen';
import newsdetail from './src/Screen/newsdetail';
import applicablefilter from './src/Screen/applicablefilter';
import RegisterScreen from './src/Screen/RegisterScreen';
import HomeScreen from './src/Screen/DrawerScreens/HomeScreen';
import profile from './src/Screen/DrawerScreens/profile';
import follow from './src/Screen/DrawerScreens/follow';
import following from './src/Screen/DrawerScreens/following';
import Cameras from './src/Screen/Cameras';
import settings from './src/Screen/settings/settings';
import contact from './src/Screen/settings/contact';
import accountinfo from './src/Screen/settings/Accountinfo';
import notification from './src/Screen/settings/notification';
import privacypolicy from './src/Screen/settings/privacypolicy';
import termscondition from './src/Screen/settings/termsandcondition';
import promoters from './src/Screen/settings/promoters';

import Gallerys from './src/Screen/gallery';
import SettingScreen from './src/Screen/DrawerScreens/SettingScreen';
import forgotpassword from './src/Screen/forgotpassword';
import ChangePasswordScreen from './src/Screen/ChangePasswordScreen';
import tab from './src/Screen/DrawerScreens/tab';
import notify from './src/Screen/notify'
import Messages from './src/Screen/Messages'
import Publishscreen from './src/Screen/publish'
import PromoterScreen from './src/Screen/PromoterScreen';
import PeopleScreen from './src/Screen/PeopleScreen'
import people_follow from './src/Screen/DrawerScreens/people_follow'
import people_following from './src/Screen/DrawerScreens/people_following';
import ApiConstants from './src/services/APIConstants';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import LiveStreamScreen from './src/Screen/LiveStreamScreen';


const Stack = createStackNavigator();





const Auth = () => {
  // Stack Navigator for Login and Sign up Screen
  const [initialRoute, setInitialRoute] = useState('SplashScreen');
  
  let [count, setCount] = useState(0);

  
  
  setTimeout(() => {
    // getNotificationList()
  },10000)
  const getNotificationList = async () => {
   
    // this.setState({ attend_list: [] })
    // alert(this.state.page)
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        // this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "get-notification" , {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {
           
            let count = responseJson.notification_count
            AsyncStorage.setItem("count",String(count))

          }).catch(error => {
            


          })
      } else {

        
      }
    })

    




  }

  // if (loading) {
  //   return null;
  // }

  return (
    <Stack.Navigator initialRouteName={initialRoute}>
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LiveStreamScreen"
        component={LiveStreamScreen}
        options={{ headerShown: false }}
      />
     
      <Stack.Screen
        name="Messages"
        component={Messages}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SettingScreen"
        component={SettingScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="profile"
        component={profile}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PromoterScreen"
        component={PromoterScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PeopleScreen"
        component={PeopleScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="notification"
        component={notification}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="follow"
        component={follow}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="following"
        component={following}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="people_follow"
        component={people_follow}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="people_following"
        component={people_following}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="forgotpassword"
        component={forgotpassword}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="ChangePasswordScreen"
        component={ChangePasswordScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="applicablefilter"
        component={applicablefilter}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OTPscreen"
        component={OTPscreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Publishscreen"
        component={Publishscreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="newsdetail"
        component={newsdetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Gallerys"
        component={Gallerys}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="settings"
        component={settings}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="privacypolicy"
        component={privacypolicy}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="termscondition"
        component={termscondition}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="contact"
        component={contact}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="accountinfo"
        component={accountinfo}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="notifications"
        component={notify}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="promoters"
        component={promoters}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="tab"
        component={tab}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>

  );
};

/* Switch Navigator for those screens which needs to be switched only once
  and we don't want to switch back once we switch from them to the next one */
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Auth">
        {/* SplashScreen which will come once for 5 Seconds */}

        {/* Auth Navigator which includer Login Signup will come once */}
        <Stack.Screen
          name="Auth"
          component={Auth}
          options={{ headerShown: false }}
        />
        {/* <Stack.Screen
          name="tab"
          component={tab}
          options={{ headerShown: false }}
        /> */}

        <Stack.Screen
          name="Cameras"
          component={Cameras}
          options={{ headerShown: false }}
        />
        {/* Navigation Drawer as a landing page */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
