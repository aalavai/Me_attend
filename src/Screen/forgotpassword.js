// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, createRef } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Modal,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import Loader from './Components/Loader';
import font from './styles/font';
import { COLORS, FONTS, images, SIZES } from '../assets/styles/theme';
import ApiConstants from '../services/APIConstants';


const forgotpassword = ({ navigation }) => {

  const [email, setEmail] = useState("");
  var email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const [dialog_visible, setDialogVisible] = useState(false);
  const [dialog_message, setDialogMessage] = useState("");
  const [loader_visible, setLoaderVisibility] = useState(false);
  const [email_validation_msg, setEmailValidationVisible] = useState(false)
  const sendMail = () => {

    if (email_reg.test(email) == false) {

      setEmailValidationVisible(true)
    } else {
      setLoaderVisibility(true)
      fetch(ApiConstants.url + "user-forgot-password", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email
        })
      }).then(response => response.json())
        .then(responseJson => {
          console.debug("sendEmal", responseJson)
          const status = responseJson.status
          if (status == "success") {

            navigation.navigate('OTPscreen', { email: email, type: "1" })
            setLoaderVisibility(false)
          } else {
            setLoaderVisibility(false)
            Toast.show({
              title: 'Error!',
              text: responseJson.value,

              color: '#000',
              timeColor: 'red',
              backgroundColor: COLORS.textbluedark.color,
              timing: 5000,
              position: 'bottom',
            })

          }
        })
    }


  }

  return (
    <View style={styles.mainBody}>
      <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

      </Root>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
        }}><View style={{ alignItems: 'center', position: 'absolute', top: 0, flex: 1, left: 0, right: 0 }}>
          <Image
            source={images.large_logo_icon}
            style={{
              resizeMode: "contain",
              margin: 30,
              height: 30
            }}
          />
        </View>

        <View>
          <KeyboardAvoidingView enabled>
            <Text
              style={[
                font.bold,
                styles.HeadText
              ]}
            >
              Forgot Password
            </Text>
            <View style={styles.SectionStyle}>
              <TextInput
                style={[styles.inputStyle, font.regular]}
                onChangeText={(text) => {
                  setEmail(text)
                  setEmailValidationVisible(false)
                }}
                placeholder="Enter Your Email" //dummy@abc.com
                placeholderTextColor="#747474"
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                underlineColorAndroid="#f000"
                blurOnSubmit={false}
              />
            </View>
            {
              email_validation_msg ?
                <View style={styles.validationText}>
                  <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Please Enter A Valid Email Address</Text>
                </View> :
                <View></View>
            }
            <TouchableOpacity
              onPress={() => sendMail()}
              style={styles.buttonStyle}
              activeOpacity={0.5}
            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Verfy Email</Text>
            </TouchableOpacity>


          </KeyboardAvoidingView>
        </View>
      </ScrollView>
      {
        loader_visible == true ? <Loader /> : <View></View>
      }
      <Modal
        activeOpacity={1}
        transparent={true}
        visible={dialog_visible}
      >
        <TouchableOpacity
          onPress={() => setDialogVisible(false)}
          activeOpacity={1}
          style={{
            backgroundColor: "#000000aa",
            flexDirection: 'column',
            height: SIZES.height,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <TouchableOpacity
            activeOpacity={1}
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              width: SIZES.width / 1.5,
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: COLORS.textblue.color,
              minHeight: 150,
              maxHeight: 150,
              borderRadius: 10,
              shadowColor: '#4e4f72',
              shadowOpacity: 0.2,
              shadowRadius: 30,
              shadowOffset: {
                height: 0,
                width: 0,
              },
              elevation: 30,
            }}>

            <ScrollView
              contentContainerStyle={{
                alignSelf: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
                height: "100%"

              }}
            >

              <Text style={{ marginTop: 10, padding: 10, textAlign: 'justify', ...FONTS.regularsizeRegular }}>
                {dialog_message}
              </Text>



            </ScrollView>
            <TouchableOpacity
              style={{
                position: 'absolute',
                top: 0,
                right: 0
              }}
              onPress={() => setDialogVisible(false)}
            >
              <Image source={images.cancel_icon} style={{
                width: 20,
                height: 20,
                tintColor: COLORS.textblue,
                alignSelf: 'flex-end',
                marginTop: 10,
                marginRight: 10
              }} />
            </TouchableOpacity>
          </TouchableOpacity>

        </TouchableOpacity>
      </Modal>

    </View>
  );
};
export default forgotpassword;

const styles = StyleSheet.create({
  HeadText: {
    color: '#fff',
    fontSize: 18,
    marginLeft: 35
  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  validationText: {
    marginLeft: 35,
    marginRight: 35,
    flexDirection: 'column',

  },
  buttonStyle: {
    backgroundColor: '#56D6BE',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 46,
    alignItems: 'center',
    borderRadius: 8,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 15,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    paddingVertical: 13,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'white',
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
});
