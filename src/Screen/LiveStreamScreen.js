// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, createRef, useEffect, useRef } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    Modal, Alert,
    BackHandler,
    TouchableOpacity,
    KeyboardAvoidingView,
    StatusBar
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import { WebView } from 'react-native-webview'

import { COLORS,  SIZES } from '../assets/styles/theme';



const LiveStreamScreen = ({  route }) => {
    const [user_token, setUserToken] = useState("")
    const [event_id, setEventId] = useState(route.params.event_id)
    useEffect(() => {
        getToken()
    },[])

    const getToken = async() =>{
        var token = await AsyncStorage.getItem("user_id")
        
        setUserToken(token)
    }
    console.log(user_token)
    console.log(event_id)
    return (
        <View style={styles.mainBody}>
            <StatusBar
                animated={true}
                backgroundColor={COLORS.textblue.color}
            />
            
            <WebView
               
               
                
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: "https://uat.meattend.com/main/viewer?token=" + user_token + "&eventId=" + event_id }}
                
                style={{ width: SIZES.width }}
            />
        </View>
    )

};
export default LiveStreamScreen;

const styles = StyleSheet.create({
    viewer: {
        flex: 1,
        display: 'flex',
        backgroundColor: '#000',
    },
    HeadText: {
        color: '#fff',
        fontSize: 18,
        marginLeft: 35
    },
    bottomspace: {
        marginBottom: 0
    },
    mainBody: {
        flex: 1,

        backgroundColor: '#fff',

    },
    validationText: {
        marginLeft: 35,
        marginRight: 35,
        flexDirection: 'column',

    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        borderRadius: 8,
        backgroundColor: "white",
        borderWidth: 1,
        borderColor: 'white',
        marginBottom: 10,
        marginLeft: 35,
        marginRight: 35,
        justifyContent: 'center',
        margin: 10,
    },
    buttonStyle: {
        backgroundColor: '#56D6BE',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#56D6BE',
        height: 46,
        alignItems: 'center',
        borderRadius: 8,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 15,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,

    },
    inputStyle: {
        flex: 1,
        color: '#747474',
        paddingLeft: 15,
        paddingRight: 15,

        borderRadius: 8,

        height: 39
    },
    forgot: {
        alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
    },
    registerTextStyle: {
        color: '#FFFFFF',
        textAlign: 'center',

        alignSelf: 'flex-start',
        marginLeft: 35,
        padding: 10,
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    remoteVideo: {
        backgroundColor: '#f2f2f2',
        height: '100%',
        width: '100%',
    },
});
