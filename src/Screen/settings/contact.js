

import React, { Component } from 'react';

import font from '../styles/font';

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

import { FONTS, images } from '../../assets/styles/theme';
import AsyncStorage from '@react-native-community/async-storage';
import ApiConstants from '../../services/APIConstants';
import Loader from '../Components/Loader';
import NetInfo from "@react-native-community/netinfo";
import { Root, Toast } from 'react-native-popup-confirm-toast'
import { SIZES, COLORS } from '../../assets/styles/theme';



export default class contact extends Component {

  constructor(props) {


    super(props);
    this.state = {
      modalVisible: false,
      search: '',
      index: 0,
      routes: [
        { key: 'first', title: 'Photos' },
        { key: 'second', title: 'Attend' },
      ],
      loader_visible: false,
      subject_validation_msg: false,
      name_validation_msg: false,
      description_validation_msg: false,
      name: "",
      subject: "",
      description: ""
    }
  }
  onCloseModel = () => {
    this.setState({
      modalVisible: false,
    });
  };

  componentDidMount = () => {
    // this.sendData()

  }

  sendData = async () => {

    var name_reg = /^[a-zA-Z]+$/;
    var token = await AsyncStorage.getItem("user_id")
    var email_id = await AsyncStorage.getItem("email_id")
    var mobile_number = await AsyncStorage.getItem("mobile_number");
    let subject = this.state.subject
    let name = this.state.name;
    let description = this.state.description
    if (subject.length == 0) {
      this.setState({ subject_validation_msg: true })
    } else if (name_reg.test(name) == false) {
      this.setState({ name_validation_msg: true })
    } else if (description.length == 0) {
      this.setState({ description_validation_msg: true })
    } else {
      console.log(email_id + ", " + mobile_number)
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ loader_visible: true })

          fetch(ApiConstants.url + "contact-us", {
            method: "POST",
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({
              name: name,
              subject: subject,
              email: email_id,
              mobile_number: mobile_number,
              message: description
            })
          }).then(response => response.json())
            .then(responseJson => {
              this.setState({ loader_visible: false })
              console.log("yyyy", responseJson.status)
              let status = responseJson.status
              let message = responseJson.value
              if (status == "success") {

                Toast.show({
                  title: 'Success',
                  text: message,

                  color: '#000',
                  timeColor: COLORS.textgreen.color,
                  backgroundColor: COLORS.textblue.color,
                  timing: 5000,
                  position: 'bottom',
                })

              }



            }).catch(error => {
              // alert("ree" + responseJson)
              this.setState({ loader_visible: false })


            })
        } else {

          Toast.show({
            title: 'Network Error!',
            text: "",
            color: '#000',
            timeColor: 'red',
            backgroundColor: COLORS.textblue.color,
            timing: 5000,
            position: 'bottom',
          })
        }
      })
    }

  }


  render() {


    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

        </Root>
        <View style={{ flex: 1, backgroundColor: "#fff" }}>

          <View style={[styles.rowgroup, styles.item, styles.centercontent]}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={{ position: "absolute", left: 5, height: 40, width: 40, justifyContent: 'center' }}>
              <Image source={images.prevarrow_icon}
                style={{
                  resizeMode: "contain", height: 15,
                  width: 9
                }}
              />

            </TouchableOpacity>
            <View style={{ position: "relative", justifyContent: "center" }}>
              <Text style={[font.bold, font.textblue, font.sizeLarge]}>Contact Us</Text>
            </View>



          </View>
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium
            ]}
          >
            Subject
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <TextInput
              onSubmitEditing={() => this.name_input.focus()}
              style={[styles.inputStyle, font.regular]}
              placeholder="Enter Subject" //12345
              placeholderTextColor="#747474"
              keyboardType="default"
              blurOnSubmit={false}
              onChangeText={(text) => {
                this.setState({ subject: text })
                this.setState({ subject_validation_msg: false })
              }}

              returnKeyType="next"
            />
          </View>
          {
            this.state.subject_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Subject is required</Text>
              </View> :
              <View></View>
          }
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium

            ]}
          >
            Name
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox]}>
            <TextInput
              ref={(input) => { this.name_input = input; }}
              onSubmitEditing={() => this.description_input.focus()}
              style={[styles.inputStyle, font.regular]}
              placeholder="Enter Name" //12345
              placeholderTextColor="#747474"
              keyboardType="default"
              blurOnSubmit={false}
              onChangeText={(text) => {
                this.setState({ name: text })
                this.setState({ name_validation_msg: false })
              }}

              returnKeyType="next"
            />
          </View>
          {
            this.state.name_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Name is required</Text>
              </View> :
              <View></View>
          }
          <Text
            style={[
              font.bold,
              styles.HeadText,
              font.textblue,
              font.sizeMedium
            ]}
          >
            Description
          </Text>
          <View style={[styles.SectionStyle, styles.shadowbox, styles.minhet]}>
            <TextInput
              style={[styles.descrptioninputStyle, font.regular, styles.minhet2]}
              ref={(input) => { this.description_input = input; }}
              placeholder="Enter Description" //12345
              placeholderTextColor="#747474"
              keyboardType="default"
              blurOnSubmit
              multiline={true}
              onChangeText={(text) => {
                this.setState({ description: text })
                this.setState({ description_validation_msg: false })
              }}

            />
          </View>
          {
            this.state.description_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Description is required</Text>
              </View> :
              <View></View>
          }
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <TouchableOpacity
              onPress={this.sendData}
              style={styles.buttonStyle}
              activeOpacity={0.5}
            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Submit</Text>
            </TouchableOpacity>
          </View>

        </View>

        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }

      </SafeAreaView>
    );
  }
};


const styles = StyleSheet.create({
  minhet2: {
    alignItems: "flex-start",
    alignSelf: "flex-start"
  },
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  topspace: {
    top: -20,
  },
  minhet: {
    minHeight: 200
  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: 24,
    shadowColor: "#444444"
  },
  validationText: {
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'column',
    marginBottom: 15
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 20
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "contain",
    height: 81,
    width: 81,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 15,
    marginLeft: 20,
    marginRight: 20,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 40,
    alignItems: 'center',
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 15,
    width: 100,
    marginHorizontal: 20
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 10,
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    width: "100%",

    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  descrptioninputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
    width: "100%",
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    paddingBottom: 0
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
