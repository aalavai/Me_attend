// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, createRef, useEffect, useRef } from 'react';
import { Dropdown} from 'react-native-element-dropdown';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import font from './styles/font';
import {
  StyleSheet,
  TextInput,
  Picker,
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  Modal,
  TouchableOpacity,
  ScrollView,
  StatusBar
} from 'react-native';

import Loader from './Components/Loader';
import { COLORS, FONTS, images, SIZES } from '../assets/styles/theme';
import ApiConstants from '../services/APIConstants';

const data = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'other' },
];

const RegisterScreen = ({ navigation }) => {

  const email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  var password_reg = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
  var age_reg = /^[0-9]+$/;
  var name_reg = /^[a-zA-Z]+$/;

  const [loader_visible, setLoaderVisibility] = useState(false);
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [age, setAge] = useState("");
  const [city, setCity] = useState("");
  const [gender, setGender] = useState("male");
  const [country, setCountry] = useState("");
  const [email, setEmail] = useState("");
  const [mobile_number, setMobile] = useState("");
  const [password, setPassword] = useState("");
  const [confirm_password, setConfirmPassword] = useState("");
  const [dialog_visible, setDialogVisible] = useState(false);
  const [dialog_message, setDialogMessage] = useState("");
  const [country_list, setCountryList] = useState([]);
  const [city_list, setCityList] = useState([]);

  const [first_name_validation_msg, setFirstNameValidationVisible] = useState(false)
  const [last_name_validation_msg, setLastNameValidationVisible] = useState(false)
  const [age_validation_msg, setAgeValidationVisible] = useState(false)
  const [email_validation_msg, setEmailValidationVisible] = useState(false)
  const [mobile_number_validation_msg, setMobileValidationVisible] = useState(false)
  const [password_validation_msg, setPasswordValidationVisible] = useState(false)
  const [confirm_password_validation_msg, setConfirmPasswordValidationVisible] = useState(false)

  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const ref_input4 = useRef();
  const ref_input5 = useRef();
  const ref_input6 = useRef();
  const ref_input7 = useRef();
  useEffect(() => {
    getCountry()
    if (ref_input6) {
      ref_input6.current.setNativeProps({
        style: { fontFamily: "Oxygen-Regular" }
      });
    }
    if (ref_input7) {
      ref_input7.current.setNativeProps({
        style: { fontFamily: "Oxygen-Regularr" }
      });
    }
  }, []);


  const getCountry = () => {

    fetch(ApiConstants.url + "country-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({


      })
    }).then(response => response.json())
      .then(responseJson => {
        // console.debug("country", responseJson)
        const status = responseJson.status
        if (status == "success") {

          setCountryList(responseJson.value)
          setCountry(responseJson.value[0].country_id)
          getCity(responseJson.value[0].country_id)

        } else {

        }
      })


  }

  const getCity = (country_id) => {
    fetch(ApiConstants.url + "city-list", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        country_id: country_id

      })
    }).then(response => response.json())
      .then(responseJson => {
        console.debug("city", responseJson)
        const status = responseJson.status
        if (status == "success") {

          setCityList(responseJson.value)
          setCity(responseJson.value[0].city_id)
        } else {

        }
      })

  }

  const selectCountry = (country_id) => {
    setCountry(country_id)
    getCity(country_id)
  }

  const register = () => {
    const first_name_value = first_name;
    const last_name_value = last_name;
    const age_value = age;
    const city_value = city;
    const country_value = country;
    const gender_value = gender;
    const email_value = email;
    const password_value = password;
    const mobile_number_value = mobile_number;
    const confirm_password_value = confirm_password;

    console.log(gender_value,city_value,country_value)



    if (name_reg.test(first_name_value) == false) {
      setFirstNameValidationVisible(true)
    } else if (name_reg.test(last_name_value) == false) {
      setLastNameValidationVisible(true)
    } else if (age_reg.test(age_value) == false) {
      setAgeValidationVisible(true)
    } else if (email_value.length == 0) {
      setEmailValidationVisible(true)
    } else if (email_reg.test(email_value) == false) {
      setEmailValidationVisible(true)
    }
    else if (mobile_number_value.length < 10) {
      setMobileValidationVisible(true)
    } else if (password_reg.test(password_value) == false) {
      setPasswordValidationVisible(true)

    } else if (confirm_password != password_value) {

      setConfirmPasswordValidationVisible(true)
    } else {
      setLoaderVisibility(true)



      fetch(ApiConstants.url + "save-user", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          first_name: first_name_value,
          last_name: last_name_value,
          age: age_value,
          city: city_value,
          country: country_value,
          email_id: email_value,
          mobile_number: mobile_number_value,
          password: password_value,
          password_confirmation: confirm_password_value,
          gender: gender_value




        })
      }).then(response => response.json())
        .then(responseJson => {
          console.debug("respp", responseJson)
          const status = responseJson.status
          if (status == "success") {
            setLoaderVisibility(false)
            navigation.navigate('OTPscreen', { email: email_value, type: "0" })
          } else {
            if (responseJson.hasOwnProperty("errors")) {
              if (responseJson.errors.hasOwnProperty("email_id") && responseJson.errors.hasOwnProperty("mobile_number")) {
                Toast.show({
                  title: 'Error!',
                  text: responseJson.errors.email_id + " and " + responseJson.errors.mobile_number,
                  color: '#000',
                  timeColor: 'red',
                  backgroundColor: COLORS.textbluedark.color,
                  timing: 5000,
                  position: 'bottom',
                })
              }
              else if (responseJson.errors.hasOwnProperty("email_id")) {
                Toast.show({
                  title: 'Error!',
                  text: responseJson.errors.email_id,

                  color: '#000',
                  timeColor: 'red',
                  backgroundColor: COLORS.textbluedark.color,
                  timing: 5000,
                  position: 'bottom',
                })
              } else if (responseJson.errors.hasOwnProperty("mobile_number")) {
                Toast.show({
                  title: 'Error!',
                  text: responseJson.errors.mobile_number,

                  color: '#000',
                  timeColor: 'red',
                  backgroundColor: COLORS.textbluedark.color,
                  timing: 5000,
                  position: 'bottom',
                })
              }

            }
            setLoaderVisibility(false)


          }
        })

    }




  }

  const _renderCityItem = item => {
    return (
      <View style={styles.item}>
        <Text style={styles.textItem}>{item.city}</Text>

      </View>
    );
  };
  return (
    <View style={{ flex: 1, backgroundColor: '#293272' }}>
      <StatusBar
        animated={true}
        backgroundColor={COLORS.textblue.color}
      />
      <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width }}>

      </Root>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <View style={{ alignItems: 'center' }}>
          <Image
            source={images.large_logo_icon}
            style={{
              resizeMode: "contain",
              margin: 45,
              height: 45
            }}
          />
        </View>

        <KeyboardAvoidingView enabled>
          <Text
            style={[
              font.bold,
              styles.HeadText
            ]}
          >
            Sign Up
          </Text>
          <View style={styles.SectionStyle}>
            <TextInput
              style={[font.regular, styles.inputStyle]}
              onSubmitEditing={() => ref_input2.current.focus()}
              onChangeText={(text) => {
                setFirstName(text)
                setFirstNameValidationVisible(false)
              }}
              underlineColorAndroid="#f000"
              placeholder="First Name"
              placeholderTextColor="#747474"
              autoCapitalize="sentences"
              returnKeyType="next"
              blurOnSubmit={false}
            />
          </View>
          {
            first_name_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>First Name Is Required</Text>
              </View> :
              <View></View>
          }
          <View style={styles.SectionStyle}>
            <TextInput
              ref={ref_input2}
              onSubmitEditing={() => ref_input3.current.focus()}
              style={[font.regular, styles.inputStyle]}
              onChangeText={(text) => {
                setLastName(text)
                setLastNameValidationVisible(false)
              }}
              underlineColorAndroid="#f000"
              placeholder="Last Name"
              placeholderTextColor="#747474"
              returnKeyType="next"
              blurOnSubmit={false}
            />
          </View>
          {
            last_name_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Last Name Is Required</Text>
              </View> :
              <View></View>
          }
          <View style={styles.SectionStyle}>
            <TextInput
              ref={ref_input3}
              onSubmitEditing={() => ref_input4.current.focus()}
              style={[font.regular, styles.inputStyle]}
              onChangeText={(text) => {
                setAge(text)
                setAgeValidationVisible(false)
              }}
              underlineColorAndroid="#f000"
              placeholder="Age"
              maxLength={2}
              keyboardType='numeric'
              placeholderTextColor="#747474"
              returnKeyType="next"
              blurOnSubmit={false}
            />
          </View>
          {
            age_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Age Is Required</Text>
              </View> :
              <View></View>
          }
          <View style={styles.PickerSectionStyle}>
            {/* <Picker

              selectedValue={gender}
              style={[{
                alignSelf: "center", fontSize: 10, width: "100%",
              }, styles.inputStyle, font.bold]}
              onValueChange={(itemValue, itemIndex) => setGender(itemValue)}
            >
              <Picker.Item label="Male" value="male" />
              <Picker.Item label="Female" value="female" />
              <Picker.Item label="other" value="other" />
            </Picker> */}
            <Dropdown
              style={{ width: "90%", marginLeft: 10 }}
              data={data}
              value={gender}
              searchPlaceholder="Search"
              labelField="label"
              valueField="value"
              placeholder="Select item"
              maxHeight={150}
              onChange={item => {
                setGender(item.value);
                console.log('selected', item);
              }}

              
            />
          </View>
          <View style={styles.PickerSectionStyle}>
            {/* {<Picker
              selectedValue={country}
              style={[{
                alignSelf: "center", fontSize: 10, width: "100%",
              }, styles.inputStyle, font.bold]}
              onValueChange={(itemValue, itemIndex) => selectCountry(itemValue)}
            >


              {country_list.map((item, key) =>
                <Picker.Item label={item.country} value={item.country_id} key={key} />
              )}

            </Picker>} */}
            <Dropdown
              style={{ width: "90%", marginLeft: 10 }}
              data={country_list}
              value={Number(country)}
              labelField="country"
              valueField="country_id"
              searchPlaceholder="Search"
              placeholder="Select item"
              onChange={item => {
                selectCountry(item.country_id)
                console.log('selected', item);
              }}

              
            />
          </View>
          <View style={styles.PickerSectionStyle}>
            {/* {<Picker
              selectedValue={city}
              style={[{
                alignSelf: "center", fontSize: 10, width: "100%",
              }, styles.inputStyle, font.bold]}
              onValueChange={(itemValue, itemIndex) => setCity(itemValue)}
              itemTextStyle={{ fontSize: 40 }}
            >
              {city_list.map((item, key) =>
                <Picker.Item label={item.city} value={item.city_id} key={key} style={{ fontSize: 10 }} />
              )}


            </Picker>} */}
            <Dropdown
              style={{ width: "90%", marginLeft: 10 }}
              data={city_list}
              value={Number(city)}
              labelField="city"
              valueField="city_id"
              searchPlaceholder="Search"
              placeholder="Select item"
              onChange={item => {
                setCity(item.city_id);
                console.log('selected', item);
              }}
              renderItem={item => _renderCityItem(item)}
             
            />
          </View>


          <View style={styles.SectionStyle}>
            <TextInput
              ref={ref_input4}
              onSubmitEditing={() => ref_input5.current.focus()}
              style={[font.regular, styles.inputStyle]}
              onChangeText={(text) => {
                setEmail(text)
                setEmailValidationVisible(false)
              }}
              underlineColorAndroid="#f000"
              placeholder="Email ID"
              placeholderTextColor="#747474"
              keyboardType="email-address"
              returnKeyType="next"
              blurOnSubmit={false}
            />
          </View>
          {
            email_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Please Enter A Valid Email Address</Text>
              </View> :
              <View></View>
          }
          <View style={styles.SectionStyle}>
            <TextInput
              ref={ref_input5}
              onSubmitEditing={() => ref_input6.current.focus()}
              style={[font.regular, styles.inputStyle]}
              onChangeText={(text) => {
                setMobile(text)
                setMobileValidationVisible(false)
              }}
              keyboardType='numeric'
              underlineColorAndroid="#f000"
              placeholder="Phone Number"
              placeholderTextColor="#747474"
              returnKeyType="next"
              blurOnSubmit={false}
              maxLength={15}
            />
          </View>
          {
            mobile_number_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>Please Enter A Valid Phone Number</Text>
              </View> :
              <View></View>
          }
          <View style={styles.SectionStyle}>
            <TextInput
              secureTextEntry={true}
              ref={ref_input6}
              onSubmitEditing={() => ref_input7.current.focus()}
              style={[font.regular,styles.inputStyle]}

              onChangeText={(text) => {
                setPassword(text)
                setPasswordValidationVisible(false)
              }}
              underlineColorAndroid="#f000"
              placeholder="Password"
              placeholderTextColor="#747474"


              returnKeyType="next"
              blurOnSubmit={false}
            />
          </View>
          {
            password_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>
                  Password must has at least 8 characters that
                  include atleast 1 lowercase character, 1 uppercase
                  character,1 number and 1 special character
                </Text>
              </View> :
              <View></View>
          }
          <View style={styles.SectionStyle}>
            <TextInput
              secureTextEntry={true}
              ref={ref_input7}
              style={[font.regular,styles.inputStyle]}
              onChangeText={(text) => {
                setConfirmPassword(text)
                setConfirmPasswordValidationVisible(false)
              }}
              underlineColorAndroid="#f000"
              placeholder="Confirm Password"
              placeholderTextColor="#747474"
              blurOnSubmit={false}
          
            />
          </View>
          {
            confirm_password_validation_msg ?
              <View style={styles.validationText}>
                <Text style={{ color: 'red', ...FONTS.regularfont11 }}>
                  Password Mismatch
                </Text>
              </View> :
              <View></View>
          }

          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={() => register()}
          >
            <Text style={[font.bold, styles.buttonTextStyle]}>Sign Up</Text>
          </TouchableOpacity>
          <Text
            style={[font.bold, styles.registerTextStyle]}
            onPress={() => navigation.navigate('LoginScreen')}>
            Already a member? <Text style={{ color: "#56D6BE" }}>Sign In</Text>
          </Text>
        </KeyboardAvoidingView>
      </ScrollView>
      {loader_visible ? <Loader /> : <View></View>}

      <Modal
        activeOpacity={1}
        transparent={true}
        visible={dialog_visible}
      >
        <TouchableOpacity
          onPress={() => setDialogVisible(false)}
          activeOpacity={1}
          style={{
            backgroundColor: "#000000aa",
            flexDirection: 'column',
            height: SIZES.height,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <TouchableOpacity
            activeOpacity={1}
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              width: SIZES.width / 1.5,
              backgroundColor: 'white',
              minHeight: 150,
              maxHeight: 150,
              borderRadius: 10,
              shadowColor: '#4e4f72',
              shadowOpacity: 0.2,
              shadowRadius: 30,
              shadowOffset: {
                height: 0,
                width: 0,
              },
              elevation: 30,
            }}>

            <ScrollView
              contentContainerStyle={{
                alignSelf: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
                height: "100%"

              }}
            >

              <Text style={{ marginTop: 10, padding: 10, textAlign: 'justify', ...FONTS.regularsizeRegular }}>
                {dialog_message}
              </Text>



            </ScrollView>
            <TouchableOpacity
              style={{
                position: 'absolute',
                top: 0,
                right: 0
              }}
              onPress={() => setDialogVisible(false)}
            >
              <Image source={images.cancel_icon} style={{
                width: 20,
                height: 20,
                tintColor: COLORS.textblue,
                alignSelf: 'flex-end',
                marginTop: 10,
                marginRight: 10
              }} />
            </TouchableOpacity>
          </TouchableOpacity>

        </TouchableOpacity>
      </Modal>
    </View>
  );
};
export default RegisterScreen;

const styles = StyleSheet.create({
  HeadText: {
    color: '#fff',
    fontSize: 18,
    marginLeft: 35,
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    paddingBottom: 0
  },
  textItem: {
    flex: 1,
    fontSize: 16,
  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  PickerSectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
    paddingLeft: 8
  },
  validationText: {
    marginLeft: 35,
    marginRight: 35,
    flexDirection: 'column',

  },
  buttonStyle: {
    backgroundColor: '#56D6BE',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 46,
    alignItems: 'center',
    borderRadius: 8,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 15,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'white',
  },
  confirminputStyle: {
    flex: 1,
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'white',
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
