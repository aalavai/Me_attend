// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
// Import React and Component

import React, { Component, useState } from 'react';
import font from './styles/font';
import VideoPlayer from 'react-native-video-player';
import EmojiSelector from 'react-native-emoji-selector'
import ImagePicker from 'react-native-image-crop-picker';
import Loader from './Components/Loader';
import io from 'socket.io-client'
import DocumentPicker from 'react-native-document-picker'
import moment from "moment";

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  BackHandler,
  Modal,
  Linking,
  FlatList,
  SafeAreaView,
  Keyboard,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, FONTS, images, SIZES } from '../assets/styles/theme';
import NetInfo from "@react-native-community/netinfo";
import ApiConstants from '../services/APIConstants';
import axios from 'react-native-axios'

var pos = 0

export default class Messages extends Component {

  constructor(props) {

    const containerStyle = { backgroundColor: 'white', padding: 20 };
    super(props);
    this.state = {
      modalVisible: false,
      search: '',
      show_emoji: false,
      message: "",
      mobile_number: props.route.params.mymobile,
      invite: props.route.params.invite,
      request: props.route.params.request,
      profile: props.route.params.profile,
      name: props.route.params.name,
      wait: props.route.params.wait,
      id: props.route.params.id,
      online: props.route.params.online,
      blocked: props.route.params.blocked,
      contact: props.route.params.contact,
      loader_visible: false,
      friend_mobile_number: props.route.params.mobile_number,
      index: 0,
      unblock_dialog_visible: false,
      blocked_alert: false,
      chat_token: "",
      message_list: [],
      attachment_list: [],
      msg_attachment_list: [],
      file_list: [],
      file_type_list: [],
      firstUnseenMessage: [],
      seen: "",
      page: 0,
      position: 0,
      open_attachment: false,
      menu_list_preview: false,
      is_doc: false,
      socket: io.connect('wss://chat.meattend.com/', {
        transports: ["websocket"],
      }),
      routes: [
        { key: 'first', title: 'Notifications' },
        { key: 'second', title: 'Messages' },
      ]
    }
    this.socket = null
    this.count = 0
    this.menuFlatListRef = null;
    this.FlatListRef = null;
  }
  componentDidMount() {
   

    BackHandler.addEventListener('hardwareBackPress', this.backAction);
    this.getProfile()
   
    this.startSocketConnection()
  }


  backAction = () => {
    if (this.state.show_emoji) {
      this.setState({ show_emoji: false })
    } else {
      this.props.navigation.goBack()
    }
    return true;
  };


  startSocketConnection = async () => {
    // alert("work")
    var token = await AsyncStorage.getItem("user_id")

    this.state.socket.once("connect", () => {
      let accesstoken = {
        token: token,
        is_company: false,
      };

      let datalist = []
      console.log("rrrrr", accesstoken)
      this.state.socket.emit("login", accesstoken);
      this.state.socket.on("login_response", (token) => {


        this.setState({ chat_token: token.token })



        let userdata = {
          token: token.token,
          from: this.state.mobile_number,
          to: this.state.friend_mobile_number,
          Page: 0,
          limit: 25,
        };

        this.state.socket.emit("conversation_list_request", userdata);
        this.state.socket.once("conversation_list_response", (msg) => {
          console.log("conversation:", msg.data)
          this.setState({ message_list: msg.data })
          let len = this.state.message_list.length
          if (len != 0) {
            console.log("lenght", len)
            try {

            } catch (error) {
              console.warn(err);
            }
          }

          var arr = []
          arr = msg.data

          arr.forEach((data) => {



            let value = {
              token: token.token,
              _id: data._id,
              response_user: this.state.friend_mobile_number,
            };

            this.state.socket.emit(
              "message_seen_single_request",
              value
            );

           
            this.state.socket.once(
              "message_seen_single_response",
              (response) => {
                // console.log(response);
              }
            )


          })



          let unseen = arr.find((data) => {
            if (
              (data.status == 2 || data.status == 1)
            ) {
              datalist.push(data);

            }
          })
          if (datalist.length > 0) {
            // setTimeout(() => , 500);
            this.FlatListRef.scrollToIndex({ animated: true, index: datalist.length - 1 })
          }

        
          console.log("unseenimag: ", datalist[datalist.length - 1], datalist.length)
         
        });

        this.state.socket.on("error", (msg) => {
          console.log(msg); // ERROR MESSAGE
        })

        this.state.socket.on('receive_message', msg => {

          let value = {
            token: this.state.chat_token,
            _id: msg.data._id,
            response_user: this.state.friend_mobile_number,
          };

          this.state.socket.emit(
            "message_seen_single_request",
            value
          );

          var jsonArray = []
          jsonArray.push(msg.data)
          console.log(jsonArray)
          this.setState(state => ({ message_list: [...jsonArray, ...this.state.message_list] }))



        })


        this.state.socket.on("message_seen_receive", (response) => {

          this.setState({ seen: response._id })
          this.state.message_list.forEach((data) => {
            if (data._id == response._id) {
              data.status = 3
            }
          })

          this.setState({ message_list: this.state.message_list })

        })

      })




    })
  }


  getProfile = async () => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "user-profile", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {
            this.setState({ loader_visible: false })

            let mobile_number = responseJson.data.mobile_number;
            this.setState({ mobile_number: mobile_number })



          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        alert("no internet")
      }
    })
  }


  onCloseModel = () => {
    this.setState({
      modalVisible: false,
    });
  };
  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };
  getTabWidthActive() {
    console.log("this.state.tabwidth: ", this.state.tabwidth);
    return {
      textAlign: "center",
      color: "#293272",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "regular",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }
  getTabWidthInActive() {
    console.log("this.state.tabwidth: ", this.state.tabwidth);

    return {
      textAlign: "center",
      color: "#646464",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "bold",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }

 

  updateSearch = (search) => {
    this.setState({ search });
  };

  chooseFile = () => {
    this.setState({ open_attachment: !this.state.open_attachment })
    

  }
  chooseImage = () => {
    this.setState({ attachment_list: [] })
    ImagePicker.openPicker({
      mediaType: "photo",
      multiple: true,
    }).then(file => {
      console.log(file);
      const path = file.path
      this.setState({ attachment_list: file })
      this.setState({ open_attachment: false })
      this.setState({ is_doc: false })
    }).catch(error => {
      console.log(error);
    });

  }
  chooseVideo = () => {
    this.setState({ attachment_list: [] })
    ImagePicker.openPicker({
      mediaType: "video",
      multiple: true,
    }).then(file => {
      console.log(file);
      const path = file.path
      this.setState({ attachment_list: file })
      this.setState({ is_doc: false })
      this.setState({ open_attachment: false })
    }).catch(error => {
      console.log(error);
    });



  }

  chooseDoc = async () => {
    this.setState({ attachment_list: [] })
    try {
      const results = await DocumentPicker.pick({
        type: [DocumentPicker.types.doc,
        DocumentPicker.types.pdf,
        DocumentPicker.types.ppt,
        DocumentPicker.types.pptx,
        DocumentPicker.types.zip,
        DocumentPicker.types.audio
        ]
      });

      console.log('res : ', results);


      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ loader_visible: true })
          let arr = []
          const data = new FormData()
          results.forEach((element, i) => {
            arr.push(element.type)
            const newFile = {
              uri: element.uri,
              type: element.type,
              name: "" + element.name
            }
            data.append('file', newFile)
            console.log(newFile)
          });

          let config = {
            auth: {
              username: "chat.aalavai.com",
              password: "c2db1fd389b27810d6f326c3e7acf790026f0621",
            },
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }
          axios.post("https://chat.meattend.com/api/upload", data, config)
            .then((response) => {
              this.setState({ attachment_list: [] })
              this.setState({ loader_visible: false })
              this.setState({ open_attachment: false })
              console.log(response.data.Data)
              let data = {
                token: this.state.chat_token,
                from: this.state.mobile_number,
                to: this.state.friend_mobile_number,
                message: this.state.message,
                fileUrl: response.data.Data,
                fileType: arr,
              };


              this.state.socket.emit("send_message", data);
              this.state.socket.on("send_message_response", (data) => {
                if (data.msg == "Sorry you are blocked...") {
                  console.log(data)

                  this.setState({ blocked_alert: true })
                }
                else {
                  let count = this.count++
                  console.log(count)
                  if (count == 0) {
                    console.log(data)
                    var jsonArray = []
                    jsonArray.push(data.data)
                    console.log("dsdsdstest", jsonArray)
                    this.setState(state => ({ message_list: [...jsonArray, ...this.state.message_list] }))
                    this.setState({ message: "" })
                  }
                }
                let count = this.count++
                console.log(count)
                if (count == 0) {
                  console.log(data)
                  var jsonArray = []
                  jsonArray.push(data.data)
                  console.log(jsonArray)
                  this.setState(state => ({ message_list: [...jsonArray, ...this.state.message_list] }))
                  this.setState({ message: "" })

                }

               
              })
             


            }).catch((error) => {
              console.log("saa", error)
              this.setState({ loader_visible: false })
            })

        } else {

          alert("no internet")
        }
      })

     
      let url = res.uri


      this.setState({ attachment_list: results })
      this.setState({ is_doc: true })
      this.setState({ open_attachment: false })
    } catch (err) {

    }


  }

  downloadFile = (url) => {

    Linking.openURL(url).catch((err) => {
      console.log(err)
    });


    
  }

  sendMessage = () => {
    console.log(this.state.attachment_list.length)
    if (this.state.attachment_list.length == 0) {
      if (this.state.blocked == 1) {
        this.setState({ unblock_dialog_visible: true })
      }

      if (this.state.message.length != 0 && this.state.blocked == 0 && this.state.contact != 0) {
        this.count = 0
        let data = {
          token: this.state.chat_token,
          from: this.state.mobile_number,
          to: this.state.friend_mobile_number,
          message: this.state.message,
         
        };


        this.state.socket.emit("send_message", data);
        this.state.socket.on("send_message_response", (data) => {
          if (data.msg == "Sorry you are blocked...") {
            console.log(data)

            this.setState({ blocked_alert: true })
          }
          else {
            let count = this.count++
            console.log(count)
            if (count == 0) {
              console.log(data)
              var jsonArray = []
              jsonArray.push(data.data)
              console.log(jsonArray)
              this.setState(state => ({ message_list: [...jsonArray, ...this.state.message_list] }))
              this.setState({ message: "" })

            }

           
          }
        })
      }

    } else {
      // alert("working")
      this.count = 0
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          this.setState({ loader_visible: true })
          let arr = []
          const data = new FormData()
          this.state.attachment_list.forEach((element, i) => {
            arr.push(element.mime)
            const newFile = {
              uri: element.path,
              type: element.mime,
              name: "" + element.path
            }
            data.append('file', newFile)
            console.log(newFile)
          });

          let config = {
            auth: {
              username: "chat.aalavai.com",
              password: "c2db1fd389b27810d6f326c3e7acf790026f0621",
            },
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }
          axios.post("https://chat.meattend.com/api/upload", data, config)
            .then((response) => {
              this.setState({ attachment_list: [] })
              this.setState({ loader_visible: false })
              console.log(response.data.Data)
              let data = {
                token: this.state.chat_token,
                from: this.state.mobile_number,
                to: this.state.friend_mobile_number,
                message: this.state.message,
                fileUrl: response.data.Data,
                fileType: arr,
              };


              this.state.socket.emit("send_message", data);
              this.state.socket.on("send_message_response", (data) => {
                if (data.msg == "Sorry you are blocked...") {
                  console.log(data)

                  this.setState({ blocked_alert: true })
                }
                else {
                  let count = this.count++
                  console.log(count)
                  if (count == 0) {
                    console.log(data)
                    var jsonArray = []
                    jsonArray.push(data.data)
                    console.log("dsdsdstest", jsonArray)
                    this.setState(state => ({ message_list: [...jsonArray, ...this.state.message_list] }))
                    this.setState({ message: "" })
                  }
                }
                
              })
              


            }).catch((error) => {
              console.log("saa", error)
              this.setState({ loader_visible: false })
            })

        } else {

          alert("no internet")
        }
      })
    }




  }

  acceptRequest = async () => {
    let params = {
      token: this.state.chat_token,
      friend: this.state.friend_mobile_number,
      acceptStatus: true, // FOR ACCEPT

    };
    console.log(params)
    this.state.socket.emit("accept_request", params);
    this.state.socket.once("accept_request_response", (msg) => {
      this.setState({ invite: 0 })
      console.log("sasasasasas" + msg); // RESPONSE OF ACCEPT/DECLINE
      this.props.navigation.goBack()
    });

    this.state.socket.on("error", (msg) => {
      console.log(msg); // ERROR MESSAGE
    })

    this.state.socket.on("proccessing_error", (msg) => {

      console.log(msg); // ERROR MESSAGE
    })


  }
  declineRequest = async () => {
    let params = {
      token: this.state.chat_token,
      friend: this.state.friend_mobile_number,
      acceptStatus: false // FOR ACCEPT

    };
    console.log(params)
    this.state.socket.emit("accept_request", params);
    this.state.socket.once("accept_request_response", (msg) => {
      console.log("sasasasasas" + msg); // RESPONSE OF ACCEPT/DECLINE
      this.props.navigation.goBack()
    });

    this.state.socket.on("error", (msg) => {
      console.log(msg); // ERROR MESSAGE
    })

    this.state.socket.on("proccessing_error", (msg) => {

      console.log(msg); // ERROR MESSAGE
    })


  }


  getMessage = () => {
    // alert(this.state.page)
    let userdata = {
      token: this.state.chat_token,
      from: this.state.mobile_number,
      to: this.state.friend_mobile_number,
      page: this.state.page,
      limit: 10,
    };

    // console.log(userdata)

    this.state.socket.emit("conversation_list_request", userdata);
    this.state.socket.once("conversation_list_response", (msg) => {
      console.log("gcgfgdrraaeae", msg)
      // this.setState({message_list:msg.data})
      if (this.state.message_list.length >= 10) {
        this.setState(state => ({ message_list: [...this.state.message_list, ...msg.data,] }))
      }

      // this.setState({ message_list: [...this.state.message_list, msg.data]   
      // });
    });
  }

  loadMoreItem = () => {

    this.setState(state => ({ page: this.state.page + 1 }), () => this.getMessage())
  }
  sendInvite = () => {
    let data = {
      token: this.state.chat_token,
      friend: this.state.friend_mobile_number,
    };
    this.state.socket.emit("friend_request", data);
    this.state.socket.on("friend_request_response", (msg) => {
      console.log(msg)
      this.props.navigation.goBack()
    });
    this.state.socket.on("error", (msg) => {
      console.log(msg); // ERROR MESSAGE
    });
    this.state.socket.on("proccessing_error", (msg) => {
      console.log(msg); // ERROR MESSAGE
    })
  }


  unBlockContact = () => {

    let userdata = {
      token: this.state.chat_token,
      block_user: this.state.friend_mobile_number,
      blockStatus: false, // FOR BLOCK
      //  blockStatus: true, // FOR UNBLOCK
    }
    // alert(this.state.chat_token)

    this.state.socket.emit("block_request", userdata);
    this.state.socket.once("block_response", (msg) => {
      this.setState({ unblock_dialog_visible: false })
      this.setState({ blocked: 0 })
      console.log(msg)

    });
  }

  renderItem = ({ item }) => {
    return (
      <View>
        {/* style={{ transform: [{ scaleY: -1 }]}}> */}

        {item.senderId != this.state.mobile_number ?
          <View style={[styles.msgwidthspace, styles.item]}>

            <View style={{ justifyContent: "flex-start", alignItems: "flex-start", minHeight: 50, flexWrap: 'wrap' }}>

              {item.fileUrl.length == 1 ?
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ msg_attachment_list: item.fileUrl })
                    this.setState({ menu_list_preview: true })
                    this.setState({ position: 0 })
                    this.setState({ file_type_list: item.fileType })
                    pos = 0
                  }}
                >

                  {item.fileType[0].split('/')[0] == "image" ? <Image
                    style={{
                      height: 150, width: 150, borderWidth: 1,
                      borderColor: '#bfbfbf'
                    }}
                    source={{ uri: item.fileUrl[0] }}
                  ></Image> :
                    <View>
                      {item.fileType[0].split('/')[0] == "video" ? <View>


                        <VideoPlayer
                          customStyles={{ playButton: { height: 0, width: 0 } }}
                          video={{ uri: item.fileUrl[0] }}
                          autoplay={false}
                          defaultMuted={true}

                          style={{
                            borderRadius: 10,
                            height: 150,
                            width: 150,

                            borderColor: 'black',
                            borderWidth: 1
                          }}
                        />
                        <View style={{
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 2,
                          justifyContent: 'center',
                          alignItems: 'center'
                        }}>

                          <Image source={images.play_button_icon}
                            style={{ height: 25, width: 25, }} />

                        </View>
                      </View> : <TouchableOpacity
                            onPress={() => this.downloadFile(item.fileUrl[0])}
                            style={{
  
                              borderRadius: 22,
                              backgroundColor: "#bfbfbf",
                              color: "#333333",
                              fontSize: 16,
  
                              paddingLeft: 5,
                              borderColor: "#293272",
                              lineHeight: 19,
  
                              paddingTop: 13,
                              paddingBottom: 13,
                            }}
                          >
                            <View
  
  
                            >
                              <Image
                                style={{
                                  height: 30,
                                  width: 30,
                                  alignSelf: 'center',
                                  tintColor: '#fff'
                                }}
                                source={images.download_icon}
                              />
                              <Text style={{
                                color: "#333333",
                                fontSize: 16,
                                marginTop: 10,
                                width: 150,
                                marginHorizontal: 8,
                                lineHeight: 19,
  
  
  
                              }}>{item.fileUrl[0].split('/')[3]}</Text>
                            </View>
  
                          </TouchableOpacity>
                        }
                    </View>



                  }
                </TouchableOpacity>


                : <View></View>}

              {item.fileUrl.length > 1 ?
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ msg_attachment_list: item.fileUrl })
                    this.setState({ menu_list_preview: true })
                    this.setState({ position: 0 })
                    this.setState({ file_type_list: item.fileType })

                    pos = 0
                  }}

                  style={{ flexDirection: 'row' }}>
                  {
                    item.fileUrl.map((itemurl, index) => (
                      index < 2 ? <View>
                        {item.fileType[index].split('/')[0] == "image" ? <Image
                          style={{
                            height: 150, width: 100, marginLeft: 2, borderWidth: 1,
                            borderColor: '#bfbfbf'
                          }}
                          source={{ uri: itemurl }}
                        ></Image> :
                          <View>
                            {item.fileType[index].split('/')[0] == "video" ? <View>


                              <VideoPlayer
                                customStyles={{ playButton: { height: 0, width: 0 } }}
                                video={{ uri: item.fileUrl[0] }}
                                autoplay={false}
                                defaultMuted={true}

                                style={{
                                  borderRadius: 10,
                                  height: 150,
                                  width: 100,

                                  borderColor: 'black',
                                  borderWidth: 1
                                }}
                              />
                              <View style={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                right: 0,
                                bottom: 2,
                                justifyContent: 'center',
                                alignItems: 'center'
                              }}>

                                <Image source={images.play_button_icon}
                                  style={{ height: 25, width: 25 }} />

                              </View>
                            </View> :
                            <TouchableOpacity
                            onPress={() => this.downloadFile(item.fileUrl[0])}
                            style={{
  
                              borderRadius: 22,
                              backgroundColor: "#bfbfbf",
                              color: "#333333",
                              fontSize: 16,
  
                              paddingLeft: 5,
                              borderColor: "#293272",
                              lineHeight: 19,
  
                              paddingTop: 13,
                              paddingBottom: 13,
                            }}
                          >
                            <View
  
  
                            >
                              <Image
                                style={{
                                  height: 30,
                                  width: 30,
                                  alignSelf: 'center',
                                  tintColor: '#fff'
                                }}
                                source={images.download_icon}
                              />
                              <Text style={{
                                color: "#333333",
                                fontSize: 16,
                                marginTop: 10,
                                width: 150,
                                marginHorizontal: 8,
                                lineHeight: 19,
  
  
  
                              }}>{item.fileUrl[0].split('/')[3]}</Text>
                            </View>
  
                          </TouchableOpacity>
                        }
  
                              {/* <Image
                                style={{
                                  height: 150,
                                  width: 100,
                                }}
                                source={images.document_attachment_icon}
                              />
                            } */}
                          </View>


                        }
                      </View> :
                        <View></View>)
                    )
                  }

                  {
                    item.fileUrl.length > 2 ? <View style={{


                      position: 'absolute',
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: 2,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}>
                      <Text style={{
                        color: '#fff', fontSize: 40,
                        fontFamily: "Oxygen-Bold",
                        textShadowColor: 'black',
                        textShadowOffset: { width: -1, height: 0 },
                        textShadowRadius: 10,

                        fontWeight: '800'
                      }}>+{item.fileUrl.length - 2}</Text>
                    </View>

                      : <View></View>}
                </TouchableOpacity>




                : <View></View>}

              {item.message.length != 0 ?
                <Text style={{
                  color: "#333333",
                  fontSize: 16,
                  padding: 5,
                  backgroundColor: "#DFE0EA",
                  borderRadius: 22,
                  paddingHorizontal: 20,
                  borderWidth: 1,
                  borderColor: "#293272",
                  lineHeight: 19,
                  paddingTop: 13,
                  paddingBottom: 8,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                  {item.message}
                </Text> : <View></View>}


              <Text style={{ color: "#747474", paddingLeft: 15, marginBottom: 0, fontSize: 11, opacity: 0.8, paddingRight: 10, alignSelf: 'flex-end' }} numberOfLines={1}>

                {moment(item.created_at).format('hh:mm A')}
              </Text>

            </View>
            

          </View> :
          <View style={[styles.colgroup, styles.item, styles.widthspace]}>

            <View style={{ justifyContent: "flex-end", alignItems: "flex-end", minHeight: 50 }}>





              {item.fileUrl.length == 1 ?
                <View>
                  {item.fileType[0].split('/')[0] == "image" ? <Image
                    style={{
                      height: 150, width: 150, borderWidth: 1,
                      borderColor: '#bfbfbf'
                    }}
                    source={{ uri: item.fileUrl[0] }}
                  ></Image> :
                    <View>

                      {item.fileType[0].split('/')[0] == "video" ?
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({ msg_attachment_list: item.fileUrl })
                            this.setState({ menu_list_preview: true })
                            this.setState({ position: 0 })
                            this.setState({ file_type_list: item.fileType })
                            pos = 0
                          }}>
                          <VideoPlayer
                            customStyles={{ playButton: { height: 0, width: 0 } }}
                            video={{ uri: item.fileUrl[0] }}
                            autoplay={false}
                            defaultMuted={true}

                            style={{
                              borderRadius: 10,
                              height: 150,
                              width: 150,

                              borderColor: 'black',
                              borderWidth: 1
                            }}
                          />
                          <View style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 2,
                            justifyContent: 'center',
                            alignItems: 'center'
                          }}>

                            <Image source={images.play_button_icon}
                              style={{ height: 25, width: 25 }} />

                          </View>
                        </TouchableOpacity> :
                        <TouchableOpacity
                          onPress={() => this.downloadFile(item.fileUrl[0])}
                          style={{

                            borderRadius: 22,
                            backgroundColor: "#A4E9DC",
                            color: "#333333",
                            fontSize: 16,

                            paddingLeft: 5,
                            borderColor: "#293272",
                            lineHeight: 19,

                            paddingTop: 13,
                            paddingBottom: 13,
                          }}
                        >
                          <View


                          >
                            <Image
                              style={{
                                height: 30,
                                width: 30,
                                alignSelf: 'center',
                                tintColor: '#fff'
                              }}
                              source={images.download_icon}
                            />
                            <Text style={{
                              color: "#333333",
                              fontSize: 16,
                              marginTop: 10,
                              width: 150,
                              marginHorizontal: 8,
                              lineHeight: 19,



                            }}>{item.fileUrl[0].split('/')[3]}</Text>
                          </View>

                        </TouchableOpacity>
                      }

                    </View>


                  }
                </View>


                : <View></View>}

              {item.fileUrl.length > 1 ?
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ msg_attachment_list: item.fileUrl })
                    this.setState({ menu_list_preview: true })
                    this.setState({ position: 0 })
                    this.setState({ file_type_list: item.fileType })

                    pos = 0
                  }}

                  style={{ flexDirection: 'row' }}>
                  {
                    item.fileUrl.map((itemurl, index) => (
                      index < 2 ? <View>
                        {item.fileType[index].split('/')[0] == "image" ? <Image
                          style={{
                            height: 150, width: 100, marginLeft: 2, borderWidth: 1,
                            borderColor: '#bfbfbf'
                          }}
                          source={{ uri: itemurl }}
                        ></Image> :
                          <View>


                            <VideoPlayer
                              customStyles={{ playButton: { height: 0, width: 0 } }}
                              video={{ uri: item.fileUrl[0] }}
                              autoplay={false}
                              defaultMuted={true}

                              style={{
                                borderRadius: 10,
                                height: 150,
                                width: 100,

                                borderColor: 'black',
                                borderWidth: 1
                              }}
                            />
                            <View style={{
                              position: 'absolute',
                              top: 0,
                              left: 0,
                              right: 0,
                              bottom: 2,
                              justifyContent: 'center',
                              alignItems: 'center'
                            }}>

                              <Image source={images.play_button_icon}
                                style={{ height: 25, width: 25 }} />

                            </View>
                          </View>

                        }
                      </View> :
                        <View></View>)
                    )
                  }

                  {
                    item.fileUrl.length > 2 ? <View style={{


                      position: 'absolute',
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: 2,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}>
                      <Text style={{
                        color: '#fff', fontSize: 40,
                        fontFamily: "Oxygen-Bold",
                        textShadowColor: 'black',
                        textShadowOffset: { width: -1, height: 0 },
                        textShadowRadius: 10,

                        fontWeight: '800'
                      }}>+{item.fileUrl.length - 2}</Text>
                    </View>

                      : <View></View>}
                </TouchableOpacity>




                : <View></View>}


              {item.message.length != 0 ? <View style={{ backgroundColor: "#A4E9DC", borderRadius: 22, }}>
                <Text style={{
                  color: "#333333",
                  fontSize: 16,
                  padding: 5,
                  paddingHorizontal: 20,
                  borderColor: "#293272",
                  lineHeight: 19,

                  paddingTop: 13,
                  paddingBottom: 8,

                }}>{item.message}</Text>

              </View> : <View></View>}
              <View style={{ flexDirection: 'row' }}>
                {item.status == 1 ? <Image
                  source={images.message_tick_icon}
                  style={{ width: 15, height: 15, alignSelf: 'center', marginRight: 5, tintColor: COLORS.lightgray.color }} />
                  : <View></View>}
                {item.status == 2 ? <Image
                  source={images.double_tick_icon}
                  style={{ width: 15, height: 15, alignSelf: 'center', marginRight: 5, tintColor: COLORS.lightgray.color }} />
                  : <View></View>}
                {item.status == 3 ? <Image
                  source={images.double_tick_icon}
                  style={{ width: 15, height: 15, alignSelf: 'center', marginRight: 5, tintColor: "#4287f5" }} />
                  : <View></View>}
                <Text style={{ color: "#747474", alignSelf: 'center', fontSize: 11, marginBottom: 0, opacity: 0.8, alignSelf: "flex-end", paddingRight: 10 }} numberOfLines={1}>

                 
                  {moment(item.created_at).format('hh:mm A')}
                </Text>

              </View>

            </View>
          </View>}


      </View>
    )




  }

  renderImageItem = ({ item }) => {
    return (
      <View>
        {!this.state.is_doc ? <Image source={{
          uri: item.path,
        }}
          style={styles.galleryitemimg}
        /> :
          <Image
            source={images.document_attachment_icon}
            style={styles.galleryitemimg}
          />
        }
      </View>

    )

  }

  menuScrollToIndexFailed(error) {
    const offset = error.averageItemLength * error.index;
    this.menuFlatListRef.scrollToOffset({ offset });
    setTimeout(() => this.menuFlatListRef.scrollToIndex({ index: error.index }), 100); // You may choose to skip this line if the above typically works well because your average item height is accurate.
  }
  msgScrollToIndexFailed(error) {
    const offset = error.averageItemLength * error.index;
    this.FlatListRef.scrollToOffset({ offset });
    setTimeout(() => this.FlatListRef.scrollToIndex({ index: error.index }), 100); // You may choose to skip this line if the above typically works well because your average item height is accurate.
  }

  render() {
    const { index, routes } = this.state
    const { search } = this.state;

    const chatMessages = this.state.message_list.map(item => (
      <Text style={{ borderWidth: 2, top: 500 }}>hh</Text>
    ));

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <TouchableOpacity activeOpacity={1}
          onPress={() => {
            this.setState({ show_emoji: false })
          }}
          style={{ flex: 1, backgroundColor: "#fff" }}>

          <View style={{
            shadowColor: 'black',
            shadowOpacity: 0.5,
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 10,
            elevation: 4,
            backgroundColor: 'white'
          }}>
            <View style={[styles.rowgroup, styles.itemheader, styles.centercontent]}>
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack()}
                style={{
                  position: "absolute", left: 5, height: 30,
                  justifyContent: "center",
                  width: 30
                }}>
                <Image source={images.prevarrow_icon}
                  style={{
                    resizeMode: "contain", height: 15,
                    width: 9
                  }}
                />

              </TouchableOpacity>
              <View style={{ position: "relative", justifyContent: "center" }}>
                <Text style={[font.bold, font.textblue, font.sizeLarge]}>  Messages </Text>
              </View>

            </View>

            <View style={[styles.rowgroup, styles.item_profile]}>
              <View>
                <Image source={{ uri: this.state.profile }}
                  style={styles.itemimg}
                />
              </View>
              <View style={{ marginRight: 40, position: "relative", flex: 1, alignContent: "center", alignSelf: "center" }}>
                <Text style={{ color: "#333333", fontSize: 16, marginBottom: 15 }, font.bold}>{this.state.name}</Text>
                {this.state.blocked == 0 ? <View>
                  {this.state.online == true ? <Text style={{ color: "#747474", fontSize: 12, marginBottom: 0, fontSize: 14, opacity: 0.8 }} numberOfLines={1}>Online</Text> :
                    <Text style={{ color: "#747474", fontSize: 12, marginTop: 6, fontSize: 14, opacity: 0.8 }} numberOfLines={1}>Offline</Text>
                  }
                </View> : <View></View>}


              </View>

            </View>
          </View>





          {this.state.wait == 1 ? <View style={{


            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Image source={{ uri: this.state.profile }}
              style={{

                resizeMode: "cover",
                height: 100,
                width: 100,
                borderRadius: 12,


              }}
            />

            <View



              style={[styles.Attendwidthspace, styles.attend_buttonStyle]}
            >
              <Text style={[styles.buttonTextStyle, font.bold]}>Waiting for accept request</Text>
            </View>
          </View> : <View></View>}





          <FlatList
            ref={ref => (this.FlatListRef = ref)}
            // onContentSizeChange={() => this.FlatListRef.scrollToEnd()}
            onScrollToIndexFailed={this.msgScrollToIndexFailed.bind(this)}
            data={this.state.message_list}
            inverted={true}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            style={{ marginBottom: 70 }}

            onEndReached={this.loadMoreItem}
            ListHeaderComponent={
              this.state.blocked == 1 ? <TouchableOpacity
                style={{ justifyContent: 'center', alignItems: 'center' }}
                onPress={() => this.setState({ unblock_dialog_visible: true })}
              >
                <Text style={{ marginBottom: 10, marginTop: 10 }}>
                  You are blocked this contact, <Text style={{ marginBottom: 10, marginTop: 10, color: COLORS.textgreen.color }}>
                    click here to unblock
                  </Text>
                </Text>
              </TouchableOpacity> : <View></View>

            }
            onEndReachedThreshold={0}
          />



          {this.state.contact != 1 ? <View style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            {this.state.invite != "1" ? <View style={{
              height: SIZES.height,
              width: SIZES.width,

              justifyContent: 'center',
              alignItems: 'center',
              flex: 1
            }}>
              <Image source={{ uri: this.state.profile }}
                style={{

                  resizeMode: "cover",
                  height: 100,
                  width: 100,
                  borderRadius: 12,


                }}
              />

              <TouchableOpacity

                onPress={() => {
                  this.sendInvite()
                }}

                style={[styles.Attendwidthspace, styles.attend_buttonStyle]}
              >
                <Text style={[styles.buttonTextStyle, font.bold]}>Send Invite</Text>
              </TouchableOpacity>
            </View>
              :
              <View style={{
                height: SIZES.height,
                width: SIZES.width,

                justifyContent: 'center',
                alignItems: 'center',
                flex: 1
              }}>
                <Image source={{ uri: this.state.profile }}
                  style={{

                    resizeMode: "cover",
                    height: 100,
                    width: 100,
                    borderRadius: 12,


                  }}
                />
                <TouchableOpacity
                  onPress={this.acceptRequest}
                  style={[styles.Attendwidthspace, styles.attend_buttonStyle]}
                >
                  <Text style={[styles.buttonTextStyle, font.bold]}>Accept Request</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.declineRequest}
                  style={[styles.Attendwidthspace, styles.attending_buttonStyle]}
                >
                  <Text style={[font.textblue, font.bold]}>Decline Request</Text>
                </TouchableOpacity>
              </View>}
          </View> : <View></View>}

          <View
            style={{
              position: "absolute",
              bottom: 70,
              backgroundColor: '#fff',
              maxHeight:400,
              width: SIZES.width,


            }}
          >
            <FlatList
             
              data={this.state.attachment_list}
              numColumns={3}
              renderItem={this.renderImageItem}
              keyExtractor={item => item.path}
              contentContainerStyle = {{ flex: 1 }}

            />
          </View>

          {this.state.open_attachment ? <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 100,
              right: 80,
              backgroundColor: '#A4E9DC',
              alignSelf: 'center',
              width: 45,
              height: 45,
              borderRadius: 30,
              justifyContent: 'center',
              alignItems: 'center',
              shadowOffset: { width: 1, height: 1 },
              shadowOpacity: 0.5,
              shadowRadius: 4,
              elevation: 4,
            }}
            onPress={() => {
              this.chooseImage()
            }}
          >
            <Image
              style={{ height: 20, width: 20, tintColor: '#fff' }}
              source={images.image_attachment_icon}
            />

          </TouchableOpacity> : <View></View>}

          {this.state.open_attachment ? <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 70,
              right: 120,
              backgroundColor: '#A4E9DC',
              alignSelf: 'center',
              width: 45,
              height: 45,
              borderRadius: 30,
              justifyContent: 'center',
              alignItems: 'center',
              shadowOffset: { width: 1, height: 1 },
              shadowOpacity: 0.5,
              shadowRadius: 4,
              elevation: 4,
            }}

            onPress={() => {
              this.chooseVideo()
            }}
          >
            <Image
              resizeMode='contain'
              style={{ height: 20, width: 20, tintColor: '#fff' }}
              source={images.video_attachment_icon}
            />
          </TouchableOpacity> : <View></View>}

          {/* {this.state.open_attachment ? <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 70,
              right: 40,
              backgroundColor: '#A4E9DC',
              alignSelf: 'center',
              width: 45,
              height: 45,
              borderRadius: 30,
              justifyContent: 'center',
              alignItems: 'center',
              shadowOffset: { width: 1, height: 1 },
              shadowOpacity: 0.5,
              shadowRadius: 4,
              elevation: 4,
            }}

            onPress={() => {
              this.chooseDoc()
            }}
          >
            <Image
              style={{ height: 20, width: 20, tintColor: '#fff' }}
              source={images.document_attachment_icon}
            />
          </TouchableOpacity> : <View></View>} */}

          <View
            style={{
              position: "absolute",
              bottom: 0,
              backgroundColor: '#fff',
              alignSelf: 'center',
              width: SIZES.width,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              shadowOffset: { width: 1, height: 1 },
              shadowOpacity: 0.5,
              shadowRadius: 4,
              elevation: 24,
              shadowColor: "#000"
            }}
          >
            <View style={[styles.shadowbox, styles.searchSection]}>
              <TextInput numberOfLines={1}
                blurOnSubmit={true}
                onFocus={() => {
                  this.setState({ show_emoji: false })
                }}
                editable={this.state.invite == 1 ? false : true}
                value={this.state.message}
                style={[
                  font.regular,
                  font.bold,
                  styles.inputStyle,
                  styles.padding,

                ]}
                placeholder={
                  "Type here..."
                }
               
                onChangeText={(text) => {
                  this.setState({ message: text })
                }}

              />
              <TouchableOpacity
                onPress={() => {
                  this.setState({ show_emoji: true })
                  Keyboard.dismiss()
                }}
                style={[styles.searchIcon, styles.rightspace]}>
                <Image
                  style={{
                    width: 20,
                    height: 20,
                    tintColor: COLORS.textblue.color,
                  }}

                  source={images.emoji_icon}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.chooseFile()}
                style={[styles.searchIcon]}>
                <Image
                  style={{
                    width: 20,
                    height: 20,
                    tintColor: COLORS.textblue.color,
                  }}
                  source={images.pin_icon}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => { this.sendMessage() }}
            >
              <Image
                source={images.send_icon}
                style={{
                  height: 35,
                  width: 35,
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              position: "absolute",
              bottom: 80,

              alignSelf: 'center',
              width: SIZES.width,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >

          </View>

        </TouchableOpacity>
        {
          this.state.show_emoji ? <EmojiSelector onEmojiSelected={(emoji) => {
            var arr = []
            arr.push(emoji)
            this.setState({ message: this.state.message.concat(emoji) })

            // this.setState({ show_emoji: false })
          }} /> :
            <View></View>
        }


        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.menu_list_preview}
        >
          <TouchableOpacity

            activeOpacity={1}
            style={{
              backgroundColor: "black",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>



            <FlatList
              ref={(ref) => this.menuFlatListRef = ref}
              horizontal
              pagingEnabled
              onScrollToIndexFailed={this.menuScrollToIndexFailed.bind(this)}
              data={this.state.msg_attachment_list}
              renderItem={({ item, index }) => (
                <View>
                  <Text style={{ color: '#fff' }}>{ }</Text>
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                    {this.state.file_type_list[index].split('/')[0] == "image" ?
                      <ImageBackground source={{ uri: item }}
                        resizeMode="contain"
                        style={{
                          height: SIZES.height / 1.1,
                          marginVertical: 20,
                          width: SIZES.width,
                          borderRadius: 8
                        }}
                      /> :
                      <VideoPlayer
                        autoplay={this.state.video_list_preview}
                        video={{ uri: item }}
                        autoplay={false}
                        defaultMuted={true}
                        disableControlsAutoHide
                        videoHeight={300}

                        style={{
                          backgroundColor: 'black',
                          alignSelf: 'center',
                          height: 300,
                          width: SIZES.width,
                        }}
                      />
                    }




                  </View>

                </View>
              )}
              keyExtractor={(item2, index) => index}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({ menu_list_preview: false })
                this.setState({ modal_active: false })
              }}
              style={{

                top: 30,
                right: 31,
                position: 'absolute'
              }}
            >

              <Image
                source={images.cancel_icon}
                style={{
                  tintColor: 'white',
                  height: 20,
                  width: 20
                }}
              />
            </TouchableOpacity>

            <View

              style={{
                flexDirection: 'row',
                width: SIZES.width,
                alignSelf: 'center',
                height: 50,


                flex: 1,

                position: 'absolute'
              }}
            >
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != 0 ? 0 : 1}
                  onPress={() => {

                    if (pos == 1) {
                      this.setState({ position: 0 })
                    }
                    if (pos != 0) {
                      pos = pos - 1
                      this.setState({ position: pos })
                      this.menuFlatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != 0 ? <Image
                    source={images.left_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.left_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center',
                  alignItems: 'flex-end'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != this.state.msg_attachment_list.length - 1 ? 0 : 1}
                  onPress={() => {
                    if (this.state.msg_attachment_list.length - 1 > pos) {
                      pos = pos + 1
                      this.setState({ position: pos })
                      this.menuFlatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != this.state.msg_attachment_list.length - 1 ? <Image
                    source={images.right_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.right_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
            </View>

          </TouchableOpacity>
        </Modal>


        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.unblock_dialog_visible}
        >
          <TouchableOpacity
            onPress={() => this.setState({ unblock_dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 200,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',



                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Warning
                </Text>
                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.regularsizeMedium }}>
                  Unblock this contact?
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={this.unBlockContact}
                    style={styles.yes_buttonStyle}
                  >
                    <Text style={{
                      paddingRight: 10,
                      paddingLeft: 10,

                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Yes
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.attend_desable_buttonStyle}
                    onPress={() => this.setState({ unblock_dialog_visible: false })}
                  >
                    <Text style={{

                      paddingRight: 10,
                      paddingLeft: 10,
                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      No
                    </Text>
                  </TouchableOpacity>


                </View>


              </ScrollView>

            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>

        <Modal
          activeOpacity={1}
          transparent={true}
          visible={false}
        >
          <TouchableOpacity
            onPress={() => this.setState({ unblock_dialog_visible: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <TouchableOpacity
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 200,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',



                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Warning
                </Text>
                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.regularsizeMedium }}>
                  You are blocked this contact
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ unblock_dialog_visible: false })}
                    style={styles.dialog_attend_buttonStyle}
                  >
                    <Text style={{
                      paddingRight: 10,
                      paddingLeft: 10,

                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Ok
                    </Text>
                  </TouchableOpacity>



                </View>


              </ScrollView>

            </TouchableOpacity>

          </TouchableOpacity>
        </Modal>
        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.blocked_alert}
        >
          <TouchableOpacity
            onPress={() => this.setState({ blocked_alert: false })}
            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <View
              activeOpacity={1}
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                width: SIZES.width / 1.5,
                backgroundColor: 'white',
                minHeight: 150,
                maxHeight: 200,
                borderRadius: 10,
                shadowColor: '#4e4f72',
                shadowOpacity: 0.2,
                shadowRadius: 30,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
                elevation: 30,
              }}>

              <ScrollView
                contentContainerStyle={{
                  alignSelf: 'center',
                  flexDirection: 'column',



                  height: "100%"

                }}
              >

                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.boldsizeMedium }}>
                  Warning
                </Text>
                <Text style={{ marginBottom: 20, paddingTop: 20, textAlign: 'center', ...FONTS.regularsizeMedium }}>
                  This user blocked your contact
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ blocked_alert: false })}
                    style={styles.dialog_attend_buttonStyle}
                  >
                    <Text style={{
                      paddingRight: 10,
                      paddingLeft: 10,

                      textAlign: 'justify',
                      ...FONTS.regularsizeRegular,
                      color: COLORS.white.color
                    }}>
                      Ok
                    </Text>
                  </TouchableOpacity>



                </View>


              </ScrollView>

            </View>

          </TouchableOpacity>
        </Modal>
        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }
      </SafeAreaView>
    );
  }
};


const styles = StyleSheet.create({
  rightspace: {
    right: 50,
  },
  colgroup: {
    flexDirection: "column",

  },
  widthspace: {
    flexDirection: "column",
    width: "50%",
    alignSelf: 'flex-end'
  },
  msgwidthspace: {
    flexDirection: "column",
    width: "50%",

  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  Attendwidthspace: {

  },

  dialog_attend_buttonStyle: {
    backgroundColor: COLORS.textblue.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,


  },
  attend_desable_buttonStyle: {
    backgroundColor: COLORS.lightgraycolor.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10
  },

  yes_buttonStyle: {
    backgroundColor: COLORS.textblue.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,
    justifyContent: 'center',
    marginTop: 20,


  },
  topspace: {
    top: -20,
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  centercontent: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20, marginTop: 10
  },
  searchSection: {
    marginTop: 10,


    width: "75%",
    justifyContent: "flex-end",
    marginBottom: 10,
    backgroundColor: '#fff',
    height: 45,
    marginHorizontal: 10,
    borderRadius: 8,
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },

  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",

  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },


  itemimg: {
    resizeMode: "cover",
    height: 50,
    width: 50,
    marginRight: 10,
    borderRadius: 12,
    alignSelf: "flex-start",
    justifyContent: "flex-start"
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 0,
    width: 100,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 7,
    alignSelf: "center",
  },
  padding: {
    flex: 1,
    padding: 0
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    width: "85%",
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  attending_buttonStyle: {

    borderWidth: 1.5,
    borderColor: "#293272",
    borderRadius: 8,

    height: 35,
    paddingLeft: 5,
    paddingRight: 5,
    justifyContent: "center",
    marginTop: 10,
  },
  attend_buttonStyle: {
    backgroundColor: COLORS.textblue.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 35,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 8,

    marginTop: 20,


  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,

    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    paddingBottom: 0
  },
  item_profile: {
    backgroundColor: '#fff',
    padding: 10,
    marginVertical: 8,


    marginBottom: 0,
    marginTop: 0,

  },
  itemheader: {
    backgroundColor: '#fff',

    height: 40,
    marginHorizontal: 16,
    borderRadius: 8,
    marginBottom: 0,
    marginTop: 0,
    paddingBottom: 0
  },
  noborder: {
    borderBottomWidth: 0,
  },
  title: {
    fontSize: 32,
  },
  galleryitemimg: {
    resizeMode: "cover",
    height: 120,
    width: 120,
    marginLeft: 8,
    marginTop: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#bfbfbf'
  },
  searchIcon: {
    width: 20,
    height: 20,
    position: "absolute",
    tintColor: COLORS.textblue.color,
    right: 15,

    top: 15
  },

});
