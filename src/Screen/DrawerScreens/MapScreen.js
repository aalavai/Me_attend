// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
// Import React and Component

import React, { Component } from 'react';
import font from '../styles/font';
import { Dropdown } from 'react-native-element-dropdown';

import {
    StyleSheet,
    TextInput,
    View,
    Text,
    Image,
    PermissionsAndroid,
    FlatList,
    SafeAreaView,
    Modal,
    TouchableOpacity,
    ScrollView,
} from 'react-native';

import { COLORS, FONTS, images, SIZES } from '../../assets/styles/theme';
import ApiConstants from '../../services/APIConstants';
import AsyncStorage from '@react-native-community/async-storage';
import { Root, Toast } from 'react-native-popup-confirm-toast'
import NetInfo from "@react-native-community/netinfo";
import Loader from '../Components/Loader';
import Geolocation from 'react-native-geolocation-service';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import DateTimePicker from '@react-native-community/datetimepicker';
import Geolocations from '@react-native-community/geolocation';

const marker1 = React.createRef();


const slideWidth = 280;


const data = [
    { label: 'Today AM', value: 'AM' },
    { label: 'Today PM', value: 'PM' },

];

export default class MapScreen extends Component {

    constructor(props) {

       
        super(props);
        this.state = {
            modalVisible: false,
            event_id: "",
            search: '',
            loader_visible: false,
            index: 0,
            latitude: 0.0,
            longitude: 0.0,
            event_list: [],
            advanced_country_id: "",
            routes: [
                { key: 'first', title: 'Events' },
                { key: 'second', title: 'People' },
            ],
            activeIndex: 0,
            advance_filter_visible: false,
            city_name: "",
            country_list: [],
            city_list: [],
            time_value: "AM",
            city_value: "",
            country_value: "",
            date: new Date(),
            mode: 'date',
            dateshow: false,
            date_value: "",
            advanced_country_id: "",
            advanced_city_id: "",
            advance_search: false,
            country_name: "",
            center: null,
            zoom: 15,
            attend_event_list: [],
            attend_event_list_flag: false

        }


       
    }

    componentDidMount = () => {
 
        this.props.navigation.addListener('focus', e => {
            this.getLocation()
            this.getCurrentCuntry()
            this.getCityName()
            var tdate = "" + this.state.date.toISOString().split('T')[0];
            var newdate = tdate.split("-").join("-");
            this.setState({ date_value: "" + newdate })
            let current_time = this.state.date.getHours()
  
            if (current_time => 12) {
             
              this.setState({ time_value: "PM" })
              
            } else {
             
              this.setState({ time_value: "AM" })
             
            }

        });

        this.props.navigation.addListener('blur', e => {

            this.setState({ event_list: [] })
            this.setState({ attend_event_list: [] })
            this.setState({ city_list: [] })
            this.setState({ country_list: [] })
        });

        this.setState({
            center: {

                latitude: 51.519443,
                longitude: -0.126000,
            }
        });
    }

    componentWillUnmount = () => {
        this.getLocation()
        this.getCurrentCuntry()
        this.getCityName()
    }

    getCurrentCuntry = async () => {
        // alert("work"+"city")
        var token = await AsyncStorage.getItem("user_id")
        fetch(ApiConstants.url + "user-country-list", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({


            })
        }).then(response => response.json())
            .then(responseJson => {

                const status = responseJson.status
                if (status == "success") {
                    console.log(responseJson)
                    const country_id = responseJson.value[0].id
                    const country_name = responseJson.value[0].country
                    if (country_id.length != 0) {
                        this.getCurrentCity(country_id)
                        this.setState({ advanced_country_id: country_id })
                        this.setState({ country_name: country_name })
                        this.getCountry()
                    }



                } else {

                }
            })

    }

    getCityName = async () => {

        var token = await AsyncStorage.getItem("user_id")
        NetInfo.fetch().then(state => {
            if (state.isConnected) {

                fetch(ApiConstants.url + "event-list", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer' + token
                    },
                    body: JSON.stringify({

                    })
                }).then(response => response.json())
                    .then(responseJson => {

                        this.setState({ city_name: responseJson.city })

                    }).catch(error => {

                        this.setState({ loader_visible: false })


                    })
            } else {

                Toast.show({
                    title: 'Network Error!',
                    text: "",

                    color: '#000',
                    timeColor: 'red',
                    backgroundColor: "black",
                    timing: 5000,
                    position: 'bottom',
                })
            }
        })
    }

    getCountry = () => {
        // alert("work")
        fetch(ApiConstants.url + "country-list", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({


            })
        }).then(response => response.json())
            .then(responseJson => {

                const status = responseJson.status
                if (status == "success") {

                    this.setState({ country_list: responseJson.value })

                }
            })


    }

    getCurrentCity = async (country_id) => {
        // alert("work"+"city")
        var token = await AsyncStorage.getItem("user_id")
        fetch(ApiConstants.url + "user-city-list", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer' + token
            },
            body: JSON.stringify({


            })
        }).then(response => response.json())
            .then(responseJson => {

                const status = responseJson.status
                if (status == "success") {

                  
                    const city_id = responseJson.current_city
                    this.setState({ city_value: city_id })
                    this.setState({ advanced_city_id: city_id })
                    if (city_id.length != 0) {
                        this.getCity(country_id)

                    }

                } else {

                }
            })

    }

    getCity = (country_id) => {

        fetch(ApiConstants.url + "city-list", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                country_id: country_id

            })
        }).then(response => response.json())
            .then(responseJson => {
                console.debug("city", responseJson.value[0].city_id)
                const status = responseJson.status
                if (status == "success") {



                    this.setState({ city_list: responseJson.value })

                } else {

                }
            })

    }

    getLocation = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: "Permission",
                    message:
                        "App needs access to your location ",

                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (Platform.OS === 'ios') {
            const auth = await Geolocation.requestAuthorization("whenInUse");
                if(auth === "granted") {
                    Geolocation.getCurrentPosition(
                        (position) => {
    
                            this.setState({ latitude: position.coords.latitude })
                            this.setState({ longitude: position.coords.longitude })
                            this.getEventList()
                        },
                        (error) => {
                            
                            console.log(error.code, error.message);
                        },
                        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                    );
    
                }
                else {
                    console.log("denied" + granted);
                }
              }
            
            if (Platform.OS === 'android') {
                await PermissionsAndroid.request(
                  PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("granted");

                    Geolocation.getCurrentPosition(
                        (position) => {
    
                            this.setState({ latitude: position.coords.latitude })
                            this.setState({ longitude: position.coords.longitude })
                            this.getEventList()
                        },
                        (error) => {
                            
                            console.log(error.code, error.message);
                        },
                        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                    );
    
                }
                else {
                    console.log("denied" + granted);
                }
              }

        } catch (err) {
            console.warn(err);
        }
    }



    getEventList = async () => {

        var token = await AsyncStorage.getItem("user_id")
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loader_visible: true })

                fetch(ApiConstants.url + "get_near_events", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer' + token
                    },
                    body: JSON.stringify({
                        event_id: this.state.event_id
                    })
                }).then(response => response.json())
                    .then(responseJson => {
                        this.setState({ attend_event_list_flag: false })
                        console.log(responseJson)
                        const status = responseJson.status
                        let event_list_data = responseJson.data
                        this.setState({ event_list: event_list_data })
                        // alert(event_list_data)

                        if (status == "success") {


                        } else {

                        }
                        // alert(JSON.stringify(responseJson))
                        this.setState({ loader_visible: false })


                    }).catch(error => {
                        alert("ree" + responseJson)
                        this.setState({ loader_visible: false })


                    })
            } else {


            }
        })
    }

    show() {
        marker1.showCallout();
    }

    hide() {
        marker1.hideCallout();
    }

    getAttendEventList = async () => {
        this.setState({ attend_event_list_flag: true })
        this.setState({ event_list: [] })
        var token = await AsyncStorage.getItem("user_id")
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loader_visible: true })

                fetch(ApiConstants.url + "get-attend-event", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer' + token
                    },
                    body: JSON.stringify({
                        event_id: this.state.event_id
                    })
                }).then(response => response.json())
                    .then(responseJson => {
                        console.log(responseJson)

                        const status = responseJson.status
                        let event_list_data = responseJson.data
                        this.setState({ event_list: event_list_data })
                        // alert(event_list_data)
                        if (status == "success") {


                        } else {

                        }
                        // alert(JSON.stringify(responseJson))
                        this.setState({ loader_visible: false })


                    }).catch(error => {
                        alert("ree" + responseJson)
                        this.setState({ loader_visible: false })


                    })
            } else {


            }
        })
    }

    
   
    

    
   
    

    setDate = (event, date) => {


        if (event.type == "set") {
            var tdate = "" + date.toISOString().split('T')[0];
            var newdate = tdate.split("-").reverse().join("/");
            this.setState({ date_value: tdate })
            this.setState({ advance_search: true })
            if (tdate.length != 0) {

            }

            this.setState({ dateshow: false })
        } else {

            this.setState({ dateshow: false })
        }

    }

    showModal = () => {
        this.setState({
          modalVisible: true,
        });
      };


    render() {
       

        return (
            <View style={{ flex: 1}}>
                <ScrollView>
                    <View >
                        <View style={{ flex: 1}}>
                            <View style={styles.headerbg}>
                                <View style={styles.selfalign}>
                                    <Image
                                        source={images.large_logo_icon}
                                        style={{
                                            resizeMode: "contain",
                                            height: 40,
                                            width: 70,marginTop:25
                                        }}
                                    />
                                </View>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    onPress={() => {
                                        this.showModal();
                                    }}
                                    style={[styles.rowgroup, styles.stretchitem]}>
                                    <Image
                                        source={images.location_icon}
                                        style={{
                                            height: 18,
                                            width: 18,
                                            marginRight: 5,
                                            tintColor: COLORS.textgreen.color,

                                            
                                        }}
                                    />
                                    <Text style={[font.bold, styles.textstyle]}>{this.state.city_name}<Text style={[font.bold, styles.textstyle], { color: "#56D6BE", }}> . </Text></Text><Text style={[font.bold, styles.textstyle]}>Today - {this.state.time_value}</Text>
                                </TouchableOpacity>
                            </View>
                           {/* <View style={{ position: 'absolute', flexDirection: "row", top: 75 }}>
                                <View style={[styles.shadowbox, styles.searchSection]}>
                                    <TextInput numberOfLines={1}
                                        style={[
                                            font.regular,
                                            font.bold,
                                            styles.inputStyle,

                                        ]}
                                        placeholder={
                                            "Search for Events and People.."
                                        }
                                        // placeholderTextColor={[font.semibold,color.greyText,]}
                                        onChangeText={(searchString) => {

                                        }}
                                        underlineColorAndroid="transparent"
                                    />
                                    <Image
                                        style={styles.searchIcon}
                                        size
                                        source={images.search_icon}
                                    />
                                </View>
                                <View style={[styles.shadowbox, styles.squrebox]}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ advance_filter_visible: true })}
                                    >
                                        <Image
                                            style={{
                                                height: 25,
                                                width: 25,
                                                tintColor: COLORS.white.color
                                            }}
                                            source={images.filter_icon}
                                        />
                                    </TouchableOpacity>

                                </View>
                                        </View> */}

                        </View>

                        <View style={styles.container}>

                            <MapView

                                ref={ref => { this.map = ref }}
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={styles.map}
                                zoomEnabled={true}
                                minZoomLevel={10}
                                region={{
                                    latitude: this.state.latitude,
                                    longitude: this.state.longitude,
                                    latitudeDelta: 9.925100,
                                    longitudeDelta: 0.0121,

                                }}

                                onLayout={() => {
                                    this.mark.showCallout()


                                }}


                            >

                                {!this.state.attend_event_list_flag ? this.state.event_list.map((item, index) => (
                                    <Marker
                                       
                                        key={index}
                                        title={item.event_name}
                                        description={item.event_address}

                                       
                                        coordinate={{ latitude: Number(item.lat), longitude: Number(item.lon) }}>
                                        <Image style={{ height: 40, width: 40 }} source={images.map_pointer_icon} />

                                        <Callout onPress={() => {

                                            this.props.navigation.navigate('SettingScreen', { event_id: item.id })
                                        }} style={styles.bubble}>
                                            <Text style={{ ...FONTS.boldsizeMedium, alignSelf: 'center' }}>{item.event_name}</Text>
                                            <Text
                                                style={{
                                                    marginTop:5,
                                                    ...FONTS.regularsizeRegular,
                                                    alignSelf:'center',
                                                   
                                                }}
                                            >{item.event_address}</Text>
                                        </Callout>
                                    </Marker>

                                )) :
                                    this.state.event_list.map((item, index) => (
                                        <Marker


                                            key={index}
                                            coordinate={{ latitude: Number(item.lat), longitude: Number(item.lon) }}>
                                            <Image style={{ height: 40, width: 40 }} source={images.location_pin_icon} />
                                            <Callout

                                                onPress={() => {
                                                    this.props.navigation.navigate('SettingScreen', { event_id: item.id })
                                                }} style={styles.bubble}>
                                                <Text style={{ ...FONTS.boldsizeMedium }}>{item.event_name}</Text>
                                                <Text
                                                    style={{
                                                       
                                                        ...FONTS.regularsizeRegular,
                                                        alignSelf:'center'
                                                    }}
                                                >{item.event_address}</Text>
                                            </Callout>

                                        </Marker>

                                    ))


                                }

                                

                                <Marker
                                    ref={ref => { this.mark = ref; }}
                                   
                                    coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }}>
                                    <Image style={{ height: 40, width: 40 }} source={images.map_pointer_icon} />
                                    <Callout style={{ backgroundColor: "white", padding: 5, width: 100, borderRadius: 10 }}>
                                        <Text style={{ ...FONTS.boldsizeMedium, alignSelf: 'center' }}>I am here</Text>

                                    </Callout>
                                </Marker>
                               
                            </MapView>
                            <View style={{
                                position: 'absolute',
                                top: 60,
                                right: 20,
                                borderWidth: 1,
                                padding: 5,
                                borderRadius: 5,
                                borderColor: "#293272",
                                backgroundColor: "white"
                            }}>

                                <TouchableOpacity style={{
                                    marginBottom: 10
                                }}>
                                    <Image
                                        style={{
                                            height: 30,
                                            width: 30,
                                            tintColor: "#293272"
                                        }}
                                        source={images.information_icon}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ zoom: 200 })
                                        this.setState({
                                            center: {
                                                latitude: 51.519443,
                                                longitude: -1.17600,
                                            }
                                        });


                                    }}
                                    style={{
                                        marginBottom: 10
                                    }} >
                                    <Image
                                        style={{
                                            height: 30,
                                            width: 30,
                                            tintColor: "#293272"
                                        }}
                                        source={images.navigation_icon}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.getAttendEventList}>
                                    <Image
                                        style={{
                                            height: 30,
                                            width: 30,
                                            tintColor: "#293272"
                                        }}
                                        source={images.all_icon}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ position: 'absolute', flexDirection: "row", top: 100 }}>
                                <View style={[styles.shadowbox, styles.searchSection]}>
                                    <TextInput numberOfLines={1}
                                        style={[
                                            font.regular,
                                            font.bold,
                                            styles.inputStyle,

                                        ]}
                                        placeholder={
                                            "Search for Events and People.."
                                        }
                                        // placeholderTextColor={[font.semibold,color.greyText,]}
                                        onChangeText={(searchString) => {

                                        }}
                                        underlineColorAndroid="transparent"
                                    />
                                    <Image
                                        style={styles.searchIcon}
                                        size
                                        source={images.search_icon}
                                    />
                                </View>
                                <View style={[styles.shadowbox, styles.squrebox]}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ advance_filter_visible: true })}
                                    >
                                        <Image
                                            style={{
                                                height: 25,
                                                width: 25,
                                                tintColor: COLORS.white.color
                                            }}
                                            source={images.filter_icon}
                                        />
                                    </TouchableOpacity>

                                </View>
                            </View>

                    </View>

                    <Modal
                        transparent={true}
                        visible={this.state.modalVisible}

                   
                    >
                        <TouchableOpacity

                            onPress={() => this.setState({ modalVisible: false })}
                            activeOpacity={1}
                            style={{
                                backgroundColor: "#000000aa",
                                flexDirection: 'column',
                                height: SIZES.height,
                                justifyContent: 'center',

                            }}

                        >


                            <View style={{ backgroundColor: "#fff", minHeight: 300, maxHeight: 350, margin: 15, borderRadius: 10, padding: 25 }}>

                                <View style={{ flexDirection: 'row', flex: 1 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ modalVisible: false })
                                        }}
                                        style={{ marginTop: 4, flex: .2, justifyContent: 'center' }}
                                    >

                                        <Image
                                            source={images.cancel_icon}
                                            style={{
                                                tintColor: COLORS.textblue.color,
                                                height: 15,
                                                width: 15
                                            }}
                                        />
                                    </TouchableOpacity>

                                    <Text style={[font.bold, styles.poptitle]}>Event Details</Text>
                                    <View style={{ flex: .2 }}></View>
                                </View>



                                <Text style={[styles.smalltitle, font.bold]}>Location</Text>
                                <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                    <View style={{ flex: 1, paddingRight: 20 }}>
                                        <View style={[styles.CitySectionStyle, styles.shadowbox]}>
                                            <Dropdown
                                                style={{ width: "90%", marginLeft: 10 }}

                                                data={this.state.city_list}
                                                searchPlaceholder="Search"
                                                labelField="city"
                                                value={Number(this.state.city_value)}
                                                valueField="city_id"
                                                placeholder="Select item"
                                                onChange={item => {
                                                    this.setState({ city_value: item.city_id })
                                                    console.log('selected', item);
                                                    const city = item.city_id;
                                                    this.setState({ event_list: [] })
                                                    if (city.length != 0) {
                                                        this.getEventList()
                                                    }
                                                }}


                                            />
                                        </View>
                                    </View>

                                </View>
                                <Text style={[styles.smalltitle, font.bold]}>Time</Text>
                                <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                    <View style={{ flex: 1, paddingRight: 20 }}>
                                        <View style={[styles.CitySectionStyle, styles.shadowbox]}>
                                            <Dropdown
                                                maxHeight={100}
                                                style={{ width: "90%", marginLeft: 10 }}
                                                value={this.state.time_value}
                                                data={data}
                                                searchPlaceholder="Search"
                                                labelField="label"
                                                valueField="value"
                                                placeholder="Select item"
                                                onChange={item => {
                                                    this.setState({ time_value: item.value })
                                                    this.setState({ event_list: [] })
                                                    const time = item.value
                                                    if (time.length != 0) {
                                                        this.getEventList()
                                                    }

                                                    console.log('selected', item.value);
                                                }}
                                            // renderItem={item => this._renderTimeItem(item)}

                                            />
                                        </View>
                                    </View>

                                </View>
                            </View>
                        </TouchableOpacity>

                    </Modal>

                    <Modal
                        visible={this.state.advance_filter_visible}
                    >
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{
                                height: SIZES.height,
                            }}>
                            <View
                                style={{
                                    marginHorizontal: 35,
                                    height: 50,

                                }}
                            >
                                <View style={{
                                    flexDirection: 'row',
                                    flex: 1,
                                    height: 10,


                                }}>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            flex: .2,
                                            height: 30,
                                            marginTop: 30,



                                        }}
                                    >
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ advance_filter_visible: false })
                                            }}
                                            style={{
                                                justifyContent: 'center',


                                                height: 30,
                                                width: 60,

                                            }}
                                        >
                                            <Image
                                                source={images.cancel_icon}
                                                style={{
                                                    height: 15,
                                                    width: 15,
                                                    tintColor: COLORS.textblue.color,
                                                    marginTop:10
                                                   

                                                }}
                                            />
                                        </TouchableOpacity>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            flex: .6,
                                            justifyContent: 'center',
                                            height: 30,
                                            alignItems: 'center',
                                            marginTop: 30
                                        }}
                                    >
                                        <Text
                                            style={{
                                                ...FONTS.boldsizeLarge,
                                                color: COLORS.textblue.color
                                            }}
                                        >Advanced Filters</Text>
                                    </View>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            flex: .2,
                                            height: 30,
                                            marginTop: 30,
                                            alignSelf: 'center',
                                            justifyContent: 'flex-end',

                                        }}
                                    >

                                    </View>
                                </View>
                            </View>


                            <View style=
                                {{
                                    marginTop: 30,
                                    flex: 1,

                                }}
                            >
                                <Text
                                    style={{
                                        marginLeft: 35,
                                        ...FONTS.regularsizeRegular,
                                        color: COLORS.textblue.color
                                    }}
                                >
                                    City
                                </Text>
                                <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                                    <Dropdown
                                        style={{ width: "90%", marginLeft: 10 }}
                                        data={this.state.city_list}
                                        value={Number(this.state.advanced_city_id)}
                                        searchPlaceholder="Search"
                                        labelField="city"
                                        valueField="city_id"
                                        placeholder="Select item"

                                        onChange={item => {
                                            this.setState({ advanced_city_id: item.city_id })
                                            this.setState({ city_name: item.city })
                                            console.log('selected', item);
                                        }}


                                    />
                                </View>
                                <Text
                                    style={{
                                        marginLeft: 35,
                                        ...FONTS.regularsizeRegular,
                                        color: COLORS.textblue.color
                                    }}
                                >
                                    Country
                                </Text>
                                <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                                    <Dropdown
                                        style={{ width: "90%", marginLeft: 10 }}
                                        value={Number(this.state.advanced_country_id)}
                                        data={this.state.country_list}
                                        searchPlaceholder="Search"
                                        labelField="country"
                                        valueField="country_id"
                                        placeholder="Select item"

                                        onChange={item => {
                                            this.setState({ advanced_country_id: item.country_id })
                                            this.setState({ country_name: item.country })
                                            this.getCity(item.country_id)
                                            console.log('selected', item);
                                        }}


                                    />

                                </View>
                                <Text
                                    style={{
                                        marginLeft: 35,
                                        ...FONTS.regularsizeRegular,
                                        color: COLORS.textblue.color
                                    }}
                                >
                                    Date
                                </Text>
                                <View style={[styles.AdvanceFilterSectionStyle, styles.shadowbox]}>
                                    <TextInput
                                        style={[styles.inputStyle, font.regular]}
                                        value={this.state.date_value}
                                        placeholder="Select Date"
                                        placeholderTextColor="#747474"
                                        keyboardType="name-phone-pad"
                                        blurOnSubmit={false}
                                        underlineColorAndroid="#f000"

                                    />
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ dateshow: true })
                                            // alert(this.state.dateshow)
                                        }}
                                        style={{
                                            position: 'absolute',
                                            right: 0,
                                            width: 40,
                                            height: "100%",
                                            alignSelf: 'flex-end',
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Image
                                            style={{
                                                width: 20,
                                                height: 20,
                                                alignSelf: 'center',
                                                tintColor: COLORS.textblue.color
                                            }}

                                            source={images.date_pcker_icon}
                                        />
                                    </TouchableOpacity>

                                </View>



                                {this.state.dateshow == true ?
                                    (<DateTimePicker value={this.state.date}
                                        mode={this.state.mode}
                                        is24Hour={true}
                                        onTouchCancel={this.cancelled}
                                        display='default'
                                        onChange={this.setDate}
                                    ></DateTimePicker>) : (<View></View>)
                                }

                                <Text
                                    style={{
                                        marginLeft: 35,
                                        ...FONTS.regularsizeMedium,
                                        color: "#333333",
                                        fontWeight: '700',
                                        marginBottom: 30,
                                        marginTop: 30
                                    }}
                                >
                                    Categories
                                </Text>

                                <View style={{ width: SIZES.width, marginLeft: 35, }}>
                                   

                                    <FlatList

                                        numColumns={3}
                                        data={this.state.category_list}
                                        renderItem={this.renderCategoryItem}
                                        keyExtractor={item => item.id}
                                    />
                                </View>



                                <TouchableOpacity
                                    onPress={() => this.getAdvancedSearchResult()}
                                    style={{
                                        width: "85%",
                                        height: 40,

                                        backgroundColor: COLORS.textgreen.color,
                                        marginHorizontal: 20,
                                        alignSelf: 'center',
                                        borderRadius: 10,
                                        justifyContent: 'center',
                                        bottom: 35,
                                        position: 'absolute'
                                    }}
                                >
                                    <Text style={{
                                        color: "white",
                                        ...FONTS.boldsizeMedium,
                                        alignSelf: 'center'
                                    }}>
                                        Search
                                    </Text>
                                </TouchableOpacity>
                            </View>



                        </TouchableOpacity>

                    </Modal>
                </ScrollView>
                {
                    this.state.loader_visible == true ? <Loader /> : <View></View>
                }
            </View>
        );
    }
};


const styles = StyleSheet.create({
    container: {

        height: SIZES.height,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,

    },
    bubble: {
        flexDirection: 'column',
        
        alignItems:'center',
        backgroundColor: '#fff',
        borderRadius: 6,
        borderColor: '#ccc',
        borderWidth: 0.5,
        padding: 15,
        width: 150,
    },
    AdvanceFilterSectionStyle: {
        flexDirection: 'row',
        height: 50,
        borderRadius: 8,
        backgroundColor: "white",
        marginBottom: 10,
        marginLeft: 35,
        marginRight: 35,
        margin: 8,
    },
    padding: {
        paddingLeft: 0,
    },
    slide: {
        flex: 1
        // other styles for the item container
    },
    slideInnerContainer: {
        width: slideWidth,
        flex: 1
        // other styles for the inner container
    },
    bgdiv: {
        backgroundColor: "#EEFBF9", marginLeft: 20, marginTop: 10, marginHorizontal: 20, borderRadius: 8, padding: 4, marginTop: 20
    },
    smallwidth: {
        width: 100,
    },
    leftspace: {
        marginHorizontal: 20,
        letterSpacing: 0.5,
        paddingBottom: 5,
    },
    colornone: {
        backgroundColor: "#fff",
        borderColor: '#293272',
        borderWidth: 1
    },
    widthspace: {
        width: "45%"
    },
    smalltitle: {
        color: "#000000", fontSize: 14, paddingTop: 15
    },
    changetxt: {
        color: "#293272", fontSize: 14
    },
    poptitle: {
        color: "#293272",
        fontSize: 18,
        flex: .6,
        alignSelf: 'center',
        textAlign: 'center'

    },
    squrebox: {
        backgroundColor: "#56D6BE",
        width: 50,
        marginRight: 10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
        marginRight: 13
    },
    topspace: {
        top: -20,
    },
    dotStyle: {
        width: "31%",
        height: 3,

        opacity: 0.8,
        borderRadius: 0,
    },
    activeDotStyle: {
        width: "31%",
        height: 3,
        backgroundColor: '#56D6BE',
        borderRadius: 0,
    },
    searchSection: {
        justifyContent: 'center',

        backgroundColor: '#fff',
        height: 50,
        flex: 1,
        position: "relative",
        marginLeft: 14,
        marginRight: 12,
        borderRadius: 8,
    },
    shadowbox: {
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: .1,
        shadowRadius: 8,
        elevation: 24,

        shadowColor: "#444444"
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    searchIcon: {
        padding: 10,
    },
    input: {
        paddingTop: 10,
        paddingRight: 40,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },
    rowgroup: {
        flexDirection: "row",
        marginLeft: 21,
        marginTop:25
    },
    textstyle: {
        color: "#fff",
        fontSize: 15,
        alignSelf: "center"
    },
    stretchitem: {
        alignItems: "stretch",
    },
    headerbg: {
        backgroundColor: "#293272",
        height: 120,
        paddingHorizontal: 40,
        paddingBottom: 10,
        paddingLeft: 20,
        flexDirection: "row",
        alignItems: "center",



    },

    HeadText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: "bold",
        marginLeft: 35
    },
    drop: {
        backgroundColor: '#fafafa',
        color: '#747474',

    },
    bottomspace: {
        marginBottom: 0
    },
    mainBody: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#293272',
        alignContent: 'center',
    },
    itemimg: {
        resizeMode: "contain",
        height: 150,
        width: 150,
        marginRight: 10,
        alignSelf: "flex-start",
        justifyContent: "flex-start"
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        borderRadius: 8,
        backgroundColor: "white",
        marginBottom: 10,
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
    },

    buttonStyle: {
        backgroundColor: '#293272',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#56D6BE',
        height: 35,
        alignItems: 'center',
        borderRadius: 8,
        marginRight: 35,
        marginTop: 10,
        marginBottom: 0,
    },
    CitySectionStyle: {
        flexDirection: 'row',
        height: 50,
        borderRadius: 8,
        backgroundColor: "white",
        marginBottom: 10,


    },

    buttonTextStyle: {
        color: '#FFFFFF',
        fontWeight: "bold",
        fontSize: 14,
        paddingTop: 7,
        alignSelf: "center",
    },
    inputStyle: {
        color: '#747474',
        paddingLeft: 14,
        paddingTop: 16,
        paddingBottom: 16,
        fontSize: 14,
        marginRight: 40,
        borderRadius: 8,
    },
    forgot: {
        alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
    },
    registerTextStyle: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        alignSelf: 'flex-start',
        marginLeft: 35,
        padding: 10,
        paddingTop: 0
    },
    tabStyle: {
        height: 30,
        minHeight: 30,
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        fontFamily: "Oxygen-Bold",

    },
    tab: {
        backgroundColor: "#fff",
        width: "auto",
        fontFamily: "Oxygen-Bold",
        borderBottomWidth: 1,
        borderColor: "#C8C8C8"
    },
    indicator: {
        height: 4,
        backgroundColor: "#56D6BE",
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    inputIOS: {
        fontSize: 14,
        paddingVertical: 10,
        paddingHorizontal: 12,
        borderWidth: 1,
        borderColor: 'green',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 14,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 1,
        borderColor: 'blue',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    item: {
        backgroundColor: '#fff',
        padding: 10,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 8
    },
    title: {
        fontSize: 32,
    },
    searchIcon: {
        width: 18,
        height: 18,
        position: "absolute",
        right: 15,
    },

});
