// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
// Import React and Component

import { ActivityIndicator } from 'react-native-paper';
import React, { Component, useState } from 'react';
import font from '../styles/font';

import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import ReadMore from 'react-native-read-more-text';
import RBSheet from "react-native-raw-bottom-sheet";
import Loader from '../Components/Loader';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  FlatList,
  SafeAreaView,
  Modal,
  Animated,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import VideoPlayer from 'react-native-video-player';
import { ImageBackground } from 'react-native';
import Gallerys from '../gallery';
import NetInfo from "@react-native-community/netinfo";
import ApiConstants from '../../services/APIConstants';
import { COLORS, FONTS, images, SIZES } from '../../assets/styles/theme';
import AsyncStorage from '@react-native-community/async-storage';
import { Root, Toast } from 'react-native-popup-confirm-toast'
var pos = 0


// image and video




const initialLayout = { width: Dimensions.get("window").width }







export default class profile extends Component {

  constructor(props) {

    const containerStyle = { backgroundColor: 'white', padding: 20 };
    super(props);
    this.state = {
      modalVisible: false,
      search: '',
      index: 0,
      routes: [
        { key: 'first', title: 'Photos' },
        { key: 'second', title: 'Attend' },
      ],
      dialog_visible: false,
      dialog_message: "",
      loader_visible: false,
      attend_list: [],
      image_list: [],
      grid_list: [],
      video_list: [],
      age: "",
      city: "",
      country: "",
      email_id: "",
      name: "",
      followers: "",
      following: "",
      gender: "",
      id: "",
      mobile_number: "",
      profile_image: "",
      event_loader_visible: true,
      page: 1,
      event_last_page: 0,
      show_preview: false,
      age: "",
      position: 0,
      play_button_visible: true,
      auto_play: false,
      user_id: "",
      flag: 0


    }
    this.flatListRef = null;
  }


  componentDidMount = () => {
    this.props.navigation.addListener('focus', e => {
      this.getProfile()
      this.getAttentEventList();
    });
    this.props.navigation.addListener('blur', e => {

      this.setState({ attend_list: [] })
      this.setState({ page: 1 })
      this.setState({ play_button_visible: true })
      this.setState({ auto_play: false })

    });
  }

  componentWillUnmount = () => {
    this.getProfile()
    this.getAttentEventList();
  }


  getAttentEventList = async () => {

    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {


        fetch(ApiConstants.url + "attended-event-list?page=" + this.state.page, {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {

            this.setState(state => ({ attend_list: [...this.state.attend_list, ...responseJson.data.data] }))
            this.setState({ event_last_page: responseJson.data.last_page })


          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }
  getProfile = async () => {
    this.setState({ grid_list: [] })
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "user-profile", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({

          })
        }).then(response => response.json())
          .then(responseJson => {

            console.log("dsdsd", responseJson.data.promotion)
            let id = responseJson.data.id
            let age = responseJson.data.age;
            let gender = responseJson.data.gender;
            let email_id = responseJson.data.email_id;
            let mobile_number = responseJson.data.mobile_number;
            const namevalu = responseJson.data.first_name + " " + responseJson.data.last_name
            let followers = responseJson.data.followers;
            let following = responseJson.data.following;
            let profile_image = responseJson.data.profile_image;
            let list = []
            list = responseJson.data.gallery
            this.storeToken(email_id, mobile_number)
            this.setState({ profile_image: profile_image })

            this.setState({ name: namevalu })
            this.setState({ age: "" + age })
            this.setState({ gender: gender })
            this.setState({ followers: followers })
            this.setState({ following: following })
            this.setState({ loader_visible: false })
            this.setState({ image_list: responseJson.data.gallery })
            this.setState({ user_id: id })
            var i = 0
            let image_list = responseJson.data.gallery;

            if (image_list.length == 0) {
              this.setState({ flag: 0 })
            } else {
              this.setState({ flag: 1 })
            }

            for (i = 0; i < list.length; i++) {
              if (list[i].type == 0) {
                this.state.grid_list.push(ApiConstants.path + "uploads/users-gallery/" + list[i].attachement_link)

              } else {

              }


            }
          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }

  storeToken = (email_id, mobile_number) => {
    AsyncStorage.setItem("email_id", email_id)
    AsyncStorage.setItem("mobile_number", mobile_number)

  }

  onCloseModel = () => {
    this.setState({
      modalVisible: false,
    });
  };

  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };
  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={[font.sizeVeryRegular, font.textblue, font.bold]} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={[font.sizeVeryRegular, font.textblue, font.bold]} onPress={handlePress}>
        Show less
      </Text>
    );
  }


  getTabWidthActive() {
    return {
      textAlign: "center",
      color: "#293272",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "regular",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }
  getTabWidthInActive() {


    return {
      textAlign: "center",
      color: "#646464",
      height: 40,
      backgroundColor: "transparent",
      minHeight: 40,
      // fontWeight: "bold",
      fontFamily: "Oxygen-Bold",
      fontSize: 18,
    };
  }

  renderLoader = () => {
    return (
      <View style={styles.loaderStyle}>
        {this.state.event_loader_visible ? <ActivityIndicator size="small" color="#aaa" /> : <View></View>}
      </View>
    )
  }

  loadMoreItem = () => {

    var current_page = this.state.page;
    var last_page = this.state.event_last_page
    // alert(current_page + "," + last_page)
    if (current_page != last_page) {
      this.setState({ event_loader_visible: true })
      this.setState(state => ({ page: this.state.page + 1 }), () => this.getAttentEventList())
    } else {
      this.setState({ event_loader_visible: false })
    }


  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case "first":
        return <View style={{
          marginTop: 10, backgroundColor: '#fff',


          marginBottom: 50
        }}>

          {this.state.flag == 0 ? <View style={{
            height: 50,

            marginTop: 20,
            width: SIZES.width / 1.1,
            marginHorizontal: 20,
            backgroundColor: "#FFF6C6",
            borderRadius: 5,
            borderColor: "#FEF1AF",

            justifyContent: 'center',
            paddingLeft: 8
          }}>
            <Text style={{ ...FONTS.regularsizeRegular, color: '#896600', fontWeight: '400' }}>No data found</Text>
          </View> :
            <FlatList
              numColumns={3}
              contentContainerStyle={{ paddingBottom: 50, paddingHorizontal: 10 }}
              data={this.state.image_list}
              renderItem={this.renderItem}
              keyExtractor={item => item.gallery_id}
            />}


        </View>
      case "second":
        return <View style={{ flex: 1, backgroundColor: '#fff', }}>
          <FlatList

            data={this.state.attend_list}
            renderItem={this.renderItem2}
            keyExtractor={item => item.id}
            ListFooterComponent={this.renderLoader}
            onEndReached={this.loadMoreItem}
          />
        </View>
    }
  }

  updateSearch = (search) => {
    this.setState({ search });
  };


  chooseImages = () => {
    ImagePicker.openPicker({
      mediaType: "photo",
      width: 300,
      height: 300,
    }).then(images => {
      // console.log(images);
      ImgToBase64.getBase64String(images.path)
        .then(base64String => {
          // console.log(base64String);
          this.uploadImage(base64String)
        })
        .catch(err => {
          console.log(err);
        });
    }).catch(error => {
      console.log(error);
    });
  }

  uploadImage = async (base64String) => {
    var token = await AsyncStorage.getItem("user_id")
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loader_visible: true })

        fetch(ApiConstants.url + "profile-image-upload", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer' + token
          },
          body: JSON.stringify({
            profile_image: "data:image/png;base64," + base64String
          })
        }).then(response => response.json())
          .then(responseJson => {


            // console.debug("up", responseJson)
            this.getProfile()
            this.setState({ loader_visible: false })

          }).catch(error => {

            this.setState({ loader_visible: false })


          })
      } else {

        setDialogMessage("No Internet")
      }
    })
  }


  renderItem = ({ item, index }) => {
    return (
      <View style={{ marginHorizontal: 2, marginTop: 5 }}>

        {item.type == 0 ?
          <TouchableOpacity onPress={() => {
            this.setState({ show_preview: true })

            try {
              setTimeout(() => this.flatListRef.scrollToIndex({ index: index }), 1000);
              pos = index
              this.setState({ position: index })
            } catch (error) {
              console.warn(error);
            }
          }}>
            <Image source={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }}
              style={styles.galleryitemimg}
            />
          </TouchableOpacity>

          :
          //   <Video 
          //   paused={true}

          //   volume={0.0}
          //   repeat={true}
          //   playInBackground={false}
          //   resizeMode={"stretch"}
          //     poster={"https://images.unsplash.com/photo-1571501679680-de32f1e7aad4"}
          //   source={{uri: "https://appj.meattend.com/uploads/users-gallery/" + item.attachement_link}}   // Can be a URL or a local file.

          //  style={styles.galleryitemvideo} />
          <TouchableOpacity

            onPress={() => {
              this.setState({ show_preview: true })

              try {
                setTimeout(() => this.flatListRef.scrollToIndex({ index: index, animated: true }), 1000);
                this.setState({ position: index })
                pos = index
              } catch (error) {
                console.warn(err);
              }
            }}
          >
            <VideoPlayer

              video={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }}
              autoplay={false}
              defaultMuted={true}
              customStyles={{ playButton: { height: 0, width: 0 } }}
              style={styles.galleryitemvideo}
            />
            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={images.default_play_button_icon}
                style={{
                  height: 17,
                  width: 17,
                  tintColor: 'white'
                }} />
            </View>


          </TouchableOpacity>


        }




      </View>)
  }
  renderItem2 = ({ item }) => {
    return (


      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('SettingScreen', { event_id: item.id })}
        style={[styles.rowgroup, styles.item, styles.shadowbox]}>
        <View>
          <Image source={{ uri: ApiConstants.path + "images/events/" + item.event_first_image.image }}
            style={styles.itemimg}
          />
        </View>
        <View style={{ marginRight: 20, position: "relative", flex: 1 }}>
          <Text style={{ color: "#333333", ...FONTS.regularsizeMedium, marginBottom: 4, fontWeight: '700' }}>
            {item.event_name}
          </Text>
          <Text style={{ color: "#747474", marginBottom: 8, ...FONTS.regularfont11 }} numberOfLines={1}>
            {item.from_time}-{item.to_time} | {item.date_time.split("-").reverse().join("/")}
          </Text>
          <Text style={{ color: "#747474", fontWeight: '700', ...FONTS.regularsizeRegular }} numberOfLines={1}> {item.company_name}</Text>

          <View style={{ paddingTop: 8, flexDirection: "row", paddingRight: 30 }} >
            <Image
              source={images.location_icon}
              resizeMode="stretch"
              style={{
                height: 13,
                width: 13,
                marginRight: 2,
                alignSelf: "center",
                tintColor: COLORS.textgreen.color
              }}
            />

            <Text style={{ color: "#747474", fontSize: 12, }, font.regular} numberOfLines={1}> {item.event_address}</Text>

          </View>
          <View>

            {item.attend_status == "attending" ? <View style={{ flexDirection: "row", borderWidth: 1.5, borderColor: "#293272", borderRadius: 8, width: 100, paddingVertical: 6, justifyContent: "center", marginTop: 10 }}>
              <Image
                source={images.tick_icon} resizeMode="contain"
                style={{
                  marginRight: 5,
                  alignSelf: "center",
                  height: 15,
                  width: 15
                }} />
              <Text style={[font.textblue, font.bold]}>Attending</Text>
            </View> :
              <TouchableOpacity
                onPress={() => this.attendEvent(item.id)}
                style={styles.attendedbuttonStyle}
              >
                <Text style={[styles.buttonTextStyle, font.bold]}>Attended</Text>
              </TouchableOpacity>}
          </View>

        </View>
        <View style={{
          position: "absolute",
          right: 10,
          top: 10
        }}>
          <Image source={images.menu_icon}
            style={
              {
                resizeMode: "contain",
                height: 20,
                width: 20,
                tintColor: COLORS.graycolor.color

              }
            }
          />
        </View>

      </TouchableOpacity>
      // <View style={[styles.rowgroup, styles.item, styles.shadowbox]}>

      //   <View>
      //     <Image source={{ uri: "https://app.meattend.com/images/events/" + item.image }}
      //       style={styles.itemimg}
      //     />
      //   </View>
      //   <View style={{ marginRight: 40, position: "relative", flex: 1 }}>
      //     <Text style={{ color: "#333333", fontSize: 16, marginBottom: 10 }, font.bold}>
      //       {item.event_name}
      //     </Text>
      //     <Text style={{ color: "#747474", fontSize: 12, marginBottom: 8 }, font.regular} numberOfLines={1}>
      //       {item.date_time}
      //     </Text>

      //     <View style={{ paddingTop: 8, flexDirection: "row", paddingRight: 30 }} >
      //       <Image
      //         source={images.gps1_icon}
      //         style={{
      //           height: 22,
      //           width: 16,
      //           marginRight: 10,
      //           alignSelf: "center",
      //           tintColor: COLORS.textgreen.color
      //         }}
      //       />
      //       <Text style={{ color: "#747474", fontSize: 12, }, font.regular} numberOfLines={1}>
      //         {item.event_address}</Text>
      //     </View>
      //     <View style={{ flexDirection: "row", borderWidth: 1.5, borderColor: "#293272", borderRadius: 8, paddingHorizontal: 15, paddingVertical: 6, justifyContent: "center", marginTop: 10 }}>
      //       <Image
      //         source={images.tick_icon} resizeMode="contain"
      //         style={{
      //           marginRight: 5,
      //           alignSelf: "center",
      //           height: 15,
      //           width: 15
      //         }} />
      //       <Text style={[font.textblue, font.bold]}>Attending</Text>
      //     </View>
      //   </View>
      //   <View style={{
      //     position: "absolute",
      //     right: 10,
      //     top: 10
      //   }}>
      //     <Image source={images.Group25_icon}
      //       style={
      //         {
      //           resizeMode: "contain",
      //           height: 20,
      //           width: 5,

      //         }
      //       }
      //     />
      //   </View>

      // </View>
    )
  }
  scrollToIndexFailed(error) {
    const offset = error.averageItemLength * error.index;
    this.flatListRef.scrollToOffset({ offset });
    setTimeout(() => this.flatListRef.scrollToIndex({ index: error.index }), 100); // You may choose to skip this line if the above typically works well because your average item height is accurate.
  }

  render() {
    const { index, routes } = this.state
    const { search } = this.state;


    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Root style={{ position: 'absolute', height: SIZES.height, width: SIZES.width, bottom: 40 }}>

        </Root>
        <View style={{ flex: 1, backgroundColor: "#fff" }}>

          <View style={[styles.rowgroup, styles.item,]}>
            <View>

              {this.state.profile_image != null ?
                <Image source={{ uri: ApiConstants.path + "uploads/users-gallery/" + this.state.profile_image }}
                  style={styles.itemimg}
                /> :
                <Image source={{ uri: ApiConstants.path + "images/users/profile_image/profile.png" }}
                  style={styles.itemimg}
                />
              }
              <TouchableOpacity
                onPress={() => {
                  this.chooseImages()
                }}
                style={{
                  position: 'absolute',
                  bottom: 5,
                  right: 15,
                  height: 30,
                  width: 30,

                }}
              >
                <Image source={images.editing_icon}
                  style={{
                    height: 30,
                    width: 30,
                    borderWidth: 1,
                    borderColor: 'white',
                    borderRadius: 10
                  }}
                />
              </TouchableOpacity>


            </View>
            <View style={{ position: "relative", flex: 1 }}>
              <View
                style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <View
                  style={{ flexDirection: "column", width: "60%" }}
                >
                  <Text style={[font.sizeMedium, font.blackcolor, font.bold, styles.textStyle]}>
                    {this.state.name}

                  </Text>
                  <Text style={{ ...FONTS.semiboldsizeRegular }}>
                    {this.state.age},{this.state.gender}

                  </Text>
                </View>


                <TouchableOpacity onPress={() => this.props.navigation.navigate('settings')}>
                  <View style={{ flexDirection: "row", marginTop: 3 }}>
                    <Image source={images.setting_icon}
                      style={{ marginRight: 5, height: 15, width: 15, marginTop:3,tintColor: COLORS.textblue.color }} />
                    <Text style={[font.sizeVeryRegular, font.textblue, font.bold]}>Settings</Text>
                  </View>
                </TouchableOpacity>
              </View>
              {/* <Text style={[font.sizeRegular, font.graycolor, font.regular]} numberOfLines={1}>27, M</Text> */}
              {/* <Text style={[font.sizeVeryRegular,font.graycolor,font.regular,styles.rowgroup,font.linevertical]}>An Artist, Loves Sushi, Up for thrilling adventures and unforgettable times. Love to party 24x7 <Text style={[font.sizeVeryRegular,font.textblue,font.bold]}>Read More...</Text></Text> */}
              <ReadMore
                numberOfLines={3}
                renderTruncatedFooter={this._renderTruncatedFooter}
                renderRevealedFooter={this._renderRevealedFooter}
                onReady={this._handleTextReady}>
                {/* <Text style={[font.sizeVeryRegular, font.graycolor, font.regular, styles.rowgroup, font.linevertical]}>
                  An Artist, Loves Sushi, Up for thrilling adventures and unforgettable times. Love to party 24x7
                </Text> */}
              </ReadMore>
            </View>

          </View>
          <View style={{ flexDirection: "row", paddingHorizontal: 20, paddingBottom: 15 }}>
            <TouchableOpacity
              style={[styles.widthspace, styles.buttonStyle]} onPress={() => this.RBSheet.open()}
            >
              <Text style={[styles.buttonTextStyle, font.bold, font.sizeRegular]}>+ Add New</Text>
            </TouchableOpacity>
            <RBSheet
              ref={ref => {
                this.RBSheet = ref;
              }}
              height={200}
              openDuration={250}
              customStyles={{
                container: {
                  justifyContent: "center",
                  alignItems: "center",
                  borderTopLeftRadius: 25,
                  borderTopRightRadius: 25
                }
              }}
            >
              <Gallerys onChange={(e) => {
                console.log("dsd" + e)
                if (e == true) {
                  this.getProfile()
                  this.RBSheet.close()
                }
              }
              } />
            </RBSheet>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('follow', { user_id: this.state.user_id })}
              style={[styles.widthspace, styles.buttonStyle, styles.colornone, font.lightbg, font.borderradiustyle]}
            >
              <Text style={[styles.buttonTextStyle, font.regular, font.graycolor, font.sizeVeryRegular]}>{this.state.followers} Followers</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('following', { user_id: this.state.user_id })}
              style={[styles.widthspace, styles.buttonStyle, styles.colornone, font.lightbg, font.borderradiustyle]}
            >
              <Text style={[styles.buttonTextStyle, font.regular, font.graycolor, font.sizeVeryRegular]}>{this.state.following} Following</Text>
            </TouchableOpacity>
          </View>
          <TabView
            navigationState={{ index, routes }}
            renderScene={this.renderScene}
            onIndexChange={index => this.setState({ index })}
            initialLayout={{ initialLayout }}
            indicatorStyle={{ backgroundColor: '#56D6BE' }}
            activeColor={{ color: "#ff" }}
            indicatorContainerStyle={{ backgroundColor: "#fff" }}
            style={[font.regular, font.sizeSmall]}
            renderTabBar={(props) => (
              <TabBar
                {...props}
                renderLabel={({ route, focused, color }) => (
                  <Animated.Text
                    numberOfLines={1}
                    style={
                      focused
                        ? this.getTabWidthActive()
                        : this.getTabWidthInActive()
                    }
                  >
                    {route.title}
                  </Animated.Text>
                )}
                getLabelText={({ route: { title } }) => title}
                indicatorStyle={styles.indicator}
                tabStyle={[styles.tabStyle, font.semibold]}
                style={[styles.tab, font.semibold]}
              />
            )}

          />

        </View>
        <Modal
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.onCloseModel();
          }}
        // onDismiss={() => this.gotoLogin()}
        >
          <View style={{ backgroundColor: "#fff", minHeight: 300, margin: 15, borderRadius: 10, padding: 25 }}>
            <Text style={[font.bold, styles.poptitle]}>Event Details</Text>
            <Text style={[styles.smalltitle, font.bold]}>Location</Text>
            <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View style={{ flex: 1, paddingRight: 20 }}>
                <TextInput
                  style={[font.regular, styles.inputStyle, styles.shadowbox]}
                  underlineColorAndroid="#f000"
                  placeholder="London, UK"
                  placeholderTextColor="#747474"
                  autoCapitalize="sentences"
                  returnKeyType="next"
                  blurOnSubmit={false}
                />
              </View>
              <View>
                <Text style={[font.bold, styles.changetxt]}>Change</Text>
              </View>
            </View>
            <Text style={[styles.smalltitle, font.bold]}>Time</Text>
            <View style={{ marginTop: 15, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
              <View style={{ flex: 1, paddingRight: 20 }}>
                <TextInput
                  style={[font.regular, styles.inputStyle, styles.shadowbox]}
                  underlineColorAndroid="#f000"
                  placeholder="Today,Noon"
                  placeholderTextColor="#747474"
                  autoCapitalize="sentences"
                  returnKeyType="next"
                  blurOnSubmit={false}
                />
              </View>
              <View>
                <Text style={[font.bold, styles.changetxt]}>Change</Text>
              </View>
            </View>
          </View>
        </Modal>
        {
          this.state.loader_visible == true ? <Loader /> : <View></View>
        }

        <Modal
          activeOpacity={1}
          transparent={true}
          visible={this.state.show_preview}
        >
          <TouchableOpacity

            activeOpacity={1}
            style={{
              backgroundColor: "#000000aa",
              flexDirection: 'column',
              height: SIZES.height,
              justifyContent: 'center',
              alignItems: 'center'
            }}>



            <FlatList
              ref={(ref) => this.flatListRef = ref}
              horizontal
              pagingEnabled
              onScrollToIndexFailed={this.scrollToIndexFailed.bind(this)}
              data={this.state.image_list}
              renderItem={({ item }) => (
                <View>
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    {item.type == 0 ?
                      <ImageBackground source={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }} resizeMode="contain"
                        style={{
                          height: SIZES.height / 1.1,
                          marginVertical: 20,
                          width: SIZES.width,
                          borderRadius: 8
                        }}
                      /> :
                      <View>
                        <VideoPlayer
                          ref={r => this.player = r}
                          video={{ uri: ApiConstants.path + "uploads/users-gallery/" + item.attachement_link }}
                          autoplay={false}
                          defaultMuted={true}
                          disableControlsAutoHide
                          videoHeight={300}

                          style={{

                            backgroundColor: 'black',
                            alignSelf: 'center',
                            height: 300,
                            width: SIZES.width,

                          }}
                        />


                      </View>

                    }

                  </View>

                </View>
              )}
              keyExtractor={(item2, index) => index}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({ show_preview: false })
              }}
              style={{

                top: 30,
                right: 31,
                position: 'absolute'
              }}
            >

              <Image
                source={images.cancel_icon}
                style={{
                  tintColor: 'white',
                  height: 20,
                  width: 20
                }}
              />
            </TouchableOpacity>

            <View

              style={{
                flexDirection: 'row',
                width: SIZES.width,
                alignSelf: 'center',
                height: 50,


                flex: 1,

                position: 'absolute'
              }}
            >
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != 0 ? 0 : 1}
                  onPress={() => {

                    if (pos == 1) {
                      this.setState({ position: 0 })
                    }
                    if (pos != 0) {
                      pos = pos - 1
                      this.setState({ position: pos })
                      this.flatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != 0 ? <Image
                    source={images.left_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.left_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
              <View

                style={{
                  flex: .5,
                  justifyContent: 'center',
                  alignItems: 'flex-end'
                }}
              >
                <TouchableOpacity
                  activeOpacity={this.state.position != this.state.image_list.length - 1 ? 0 : 1}
                  onPress={() => {
                    if (this.state.image_list.length - 1 > pos) {
                      pos = pos + 1
                      this.setState({ position: pos })
                      this.flatListRef.scrollToIndex({ index: pos })
                    }

                  }}
                >
                  {this.state.position != this.state.image_list.length - 1 ? <Image
                    source={images.right_arrow_icon}
                    style={{
                      tintColor: 'white',
                      height: 30,
                      width: 30
                    }}
                  /> : <Image
                    source={images.right_arrow_icon}
                    style={{

                      height: 30,
                      width: 30,
                      tintColor: "#CBCBCB",
                      opacity: 0.6
                    }}
                  />}
                </TouchableOpacity>

              </View>
            </View>

          </TouchableOpacity>
        </Modal>

      </SafeAreaView>
    );
  }
};


const styles = StyleSheet.create({
  widthspace: {
    width: "100%",
    marginRight: 13,
    marginTop: 0
  },
  smalltitle: {
    color: "#000000", fontSize: 14, paddingTop: 15
  },
  changetxt: {
    color: "#293272", fontSize: 14
  },
  poptitle: {
    color: "#293272", fontSize: 18,
    marginBottom: 10
  },
  squrebox: {
    backgroundColor: "#56D6BE",
    width: 50,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
  },
  attendedbuttonStyle: {
    backgroundColor: COLORS.lightgraycolor.color,
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginRight: 35,
    marginTop: 10,
    marginBottom: 0,
    width: 100,

  },
  topspace: {
    top: -20,
  },
  bottommargin: {
    marginBottom: 20
  },
  dotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#C8C8C8',
    opacity: 0.8,
    borderRadius: 0,
  },
  activeDotStyle: {
    width: "31%",
    height: 3,
    backgroundColor: '#56D6BE',
    borderRadius: 0,
  },
  searchSection: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flex: 1,
    position: "relative",
    marginHorizontal: 20,
    borderRadius: 8,
  },
  textStyle: {
    width: "60%"
  },
  shadowbox: {
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
  },
  searchIcon: {
    padding: 10,
  },
  loaderStyle: {
    marginVertical: 16,
    alignItems: 'center',
    marginBottom: 60
  },
  input: {
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  rowgroup: {
    flexDirection: "row",
  },
  textstyle: {
    color: "#fff",
    fontSize: 15,
    alignSelf: "center"
  },
  stretchitem: {
    alignItems: "stretch"
  },
  headerbg: {
    backgroundColor: "#293272",
    height: 100,
    paddingHorizontal: 40,
    paddingLeft: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  HeadText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 35
  },
  drop: {
    backgroundColor: '#fafafa',
    color: '#747474',

  },
  bottomspace: {
    marginBottom: 0
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#293272',
    alignContent: 'center',
  },
  itemimg: {
    resizeMode: "cover",
    height: 140,
    width: 140,
    marginRight: 10,
    alignSelf: "flex-start",
    justifyContent: "flex-start",
    borderRadius: 10,
    borderWidth: 1
  },
  galleryitemimg: {
    resizeMode: "cover",
    height: 120,
    width: 120,

    alignSelf: "flex-start",
    // justifyContent: "flex-start",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#bfbfbf'
  },
  galleryitemvideo: {

    height: 120,
    width: 120,

    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#bfbfbf'
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 8,
    backgroundColor: "white",
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },

  buttonStyle: {
    backgroundColor: '#293272',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#56D6BE',
    height: 33,
    alignItems: 'center',
    borderRadius: 8,
    marginTop: -7,
    marginBottom: 0,
    width: 100,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    fontWeight: "bold",
    fontSize: 14,
    paddingTop: 4, //7
    alignSelf: "center",
  },
  inputStyle: {
    color: '#747474',
    paddingLeft: 15,
    fontSize: 14,
    paddingRight: 35,
    borderRadius: 8,
  },
  forgot: {
    alignSelf: "flex-end", marginRight: 35, color: "#56D6BE", paddingTop: 0, paddingBottom: 0
  },
  registerTextStyle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'flex-start',
    marginLeft: 35,
    padding: 10,
    paddingTop: 0
  },
  tabStyle: {
    height: 30,
    minHeight: 30,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    fontFamily: "Oxygen-Bold",

  },
  tab: {
    backgroundColor: "#fff",
    width: "auto",
    fontFamily: "Oxygen-Bold",
    borderBottomWidth: 1,
    borderColor: "#C8C8C8"
  },
  indicator: {
    height: 4,
    backgroundColor: "#56D6BE",
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
  inputIOS: {
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'green',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: 'blue',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  item: {
    backgroundColor: '#fff',
    padding: 6,
    marginVertical: 8,
    marginHorizontal: 14,
    marginBottom: 20,
    borderRadius: 8
  },
  title: {
    fontSize: 32,
  },
  searchIcon: {
    width: 18,
    height: 18,
    position: "absolute",
    right: 15,
  },

});
